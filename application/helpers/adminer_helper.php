<?php

defined('BASEPATH') or exit('No direct script access allowed');

function get_inputs($fields, $input)
{
    $data = array();
    foreach ($fields as $field) {
        if (isset($input[$field]) && $input[$field] != null) {
            $data[$field] = $input[$field];
        }
    }

    return $data;
}

function mobile_verification_otp($mobile, $email){
    $otp = rand(100000, 999999);

    // For demo purpose OTP is sent to user email
    $ci = &get_instance();    
    $mg = Mailgun\Mailgun::create('c68ed9e8cdf0359f2f202e30249c426d-1b65790d-bb108f05');
    $viewData = array(
        'otp' => $otp
    );
    $message = $ci->load->view('email/mobile_verify', $viewData, true);
    $domain = 'mg.adminer.gq';
    $params = array(
        'from'    =>  APP_NAME.'<mailgun@mg.adminer.gq>',
        'to'      =>  $email,
        'cc'      => 'Kalaivanan <kalaivanan.muthusamy97@gmail.com>',
        'subject' => 'OTP for '.APP_NAME.' Mobile number verification',
        'html'    =>  $message
    );
    // $result = $mg->messages()->send($domain, $params);

    // Write your code to send the otp to user mobile
    $response = array(
        'success' => true,
        'otp' => $otp
    );
    return $response;
}

function login_via_otp($mobile, $email)
{
    $otp = rand(100000, 999999);

    // For demo purpose OTP is sent to user email
    $ci = &get_instance();    
    $mg = Mailgun\Mailgun::create('c68ed9e8cdf0359f2f202e30249c426d-1b65790d-bb108f05');
    $viewData = array(
        'otp' => $otp
    );
    $message = $ci->load->view('email/otp_login', $viewData, true);
    $domain = 'mg.adminer.gq';
    $params = array(
        'from'    =>  APP_NAME.'<mailgun@mg.adminer.gq>',
        'to'      =>  $email,
        'cc'      => 'Kalaivanan <kalaivanan.muthusamy97@gmail.com>',
        'subject' => 'OTP for '.APP_NAME.' Mobile number verification',
        'html'    =>  $message
    );
    // $result = $mg->messages()->send($domain, $params);

    // Write your code to send the otp to user mobile
    $response = array(
        'success' => true,
        'otp' => $otp
    );
    return $response;
}

function send_email_verification_mail($email, $link){
    // Update the code to send email to the user

    $ci = &get_instance();
    
    $mg = Mailgun\Mailgun::create('c68ed9e8cdf0359f2f202e30249c426d-1b65790d-bb108f05');
    $viewData = array(
        'link' => $link
    );
    $message = $ci->load->view('email/welcome', $viewData, true);
    $domain = 'mg.adminer.gq';
    $params = array(
        'from'    =>  APP_NAME.'<mailgun@mg.adminer.gq>',
        'to'      =>  $email,
        'cc'      => 'Kalaivanan <kalaivanan.muthusamy97@gmail.com>',
        'subject' => 'Welcome to '.APP_NAME.'. Verify your email',
        'html'    =>  $message
    );
    // $result = $mg->messages()->send($domain, $params);
}

function send_password_reset_email($email, $link)
{
    // Update the code to send email to the user
    $ci = &get_instance();
    
    $mg = Mailgun\Mailgun::create('c68ed9e8cdf0359f2f202e30249c426d-1b65790d-bb108f05');
    $viewData = array(
        'link' => $link
    );
    $message = $ci->load->view('email/reset', $viewData, true);
    $domain = 'mg.adminer.gq';
    $params = array(
        'from'    =>  APP_NAME . '<mailgun@adminer.gq>',
        'to'      =>  $email,
        'cc'      => 'Kalaivanan <kalaivanan.muthusamy97@gmail.com>',
        'subject' => 'Reset your '.APP_NAME.' account password',
        'html'    =>  $message
    );
    // $result = $mg->messages()->send($domain, $params);
}

function otp_utilization_control($user_details, $settings){
    if($user_details['otp_sent'] >= $settings['settings_authentication']['max_otp_attempts'] && strtotime($user_details['last_otp_sent_on']) > strtotime('-' . $settings['settings_authentication']['otp_block_time'] . ' min')){
        $waiting_time = round((strtotime('+' . $settings['settings_authentication']['otp_block_time'] . ' min', strtotime($user_details['last_otp_sent_on'])) - time())/60);
        $waiting_time = ($waiting_time == 0) ? 'a moment' : $waiting_time.' minutes';
        $response = array(
            'status' => false,
            'message' =>  "Too many OTP sent. Wait for $waiting_time and try again"
        );
    }
    else{
        $last_otp_sent_on = date('Y-m-d H:i:s');
        if(strtotime($user_details['last_otp_sent_on']) <= strtotime('-' . $settings['settings_authentication']['otp_block_time'] . ' min')){
            $otp_sent = 1;
        }
        else{
            $otp_sent = $user_details['otp_sent'] + 1;
        }
        $update_data = array(
            'otp_sent' => $otp_sent,
            'last_otp_sent_on' => $last_otp_sent_on
        );
        $ci = &get_instance();
        $ci->load->model('Users_model');
        $model_call = $ci->Users_model->update_otp_utilization($user_details['mobile'], $update_data);
        if($model_call){
            $response = array(
                'status' => true
            );
        } else {
            $response = array(
                'status' => false,
                'message' => 'Unexpected error. Please try again'
            );
        }
        
    }
    return $response;
}

function get_ip_address()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
        return $_SERVER['HTTP_FORWARDED'];
    }

    return $_SERVER['REMOTE_ADDR'];
}

function validate_ip($ip)
{
    if (strtolower($ip) === 'unknown') {
        return false;
    }
    $ip = ip2long($ip);
    if ($ip !== false && $ip !== -1) {
        $ip = sprintf('%u', $ip);
        if ($ip >= 0 && $ip <= 50331647) {
            return false;
        }
        if ($ip >= 167772160 && $ip <= 184549375) {
            return false;
        }
        if ($ip >= 2130706432 && $ip <= 2147483647) {
            return false;
        }
        if ($ip >= 2851995648 && $ip <= 2852061183) {
            return false;
        }
        if ($ip >= 2886729728 && $ip <= 2887778303) {
            return false;
        }
        if ($ip >= 3221225984 && $ip <= 3221226239) {
            return false;
        }
        if ($ip >= 3232235520 && $ip <= 3232301055) {
            return false;
        }
        if ($ip >= 4294967040) {
            return false;
        }
    }

    return true;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; ++$i) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}
