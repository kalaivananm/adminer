<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authentication_model extends CI_Model
{
    public function is_login_allowed($role_id)
    {
        $query = $this->db->get_where('roles', array('role_id' => $role_id));
        if (!empty($query->result_array())) {
            $role_details = $query->row_array();
            if ($role_details['login_allowed'] == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function email_login($email)
    {
        $this->db->select('users.user_id, users.email, users.mobile, users.password, users.role_id, users.failed_attempts, users.status, users.mobile_verified, users.email_verified, users.first_name, users.last_name, users.last_login, users.last_login_ip, users.2fa_enabled, rls.display_name as role_name');
        $this->db->join('roles rls', 'rls.role_id=users.role_id', 'LEFT_JOIN');
        $query = $this->db->get_where('users', array('users.email' => $email));
        if ($query->num_rows() > 0) {
            $response = array(
                'status' => true,
                'user_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'status' => false,
            );
        }
        return $response;
    }

    public function set_remember_me_token($user_id, $token)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('users', array('remember_me_token' => $token));
    }

    public function check_remember_me_token($token)
    {
        $query = $this->db->get_where('users', array('remember_me_token' => $token));
        if ($query->num_rows() > 0) {
            $user_details = $query->row_array();
            $response = array(
                'status' => true,
                'user_details' => $user_details,
            );
        } else {
            $response = array(
                'status' => false,
            );
        }

        return $response;
    }

    public function log_auth_history($auth_history)
    {
        $this->db->insert('authentication_history', $auth_history);
    }

    public function log_failed_attempt($user_id, $failed_attempt)
    {
        $this->db->where('user_id', $user_id);
        $this->db->set('failed_attempts', 'failed_attempts+1', false);
        $this->db->update('users', $failed_attempt);
    }

    public function log_success_attempt($user_id, $success_attempt)
    {
        $this->db->where('user_id', $user_id);
        $this->db->set('failed_attempts', 0);
        $this->db->update('users', $success_attempt);
    }

    public function get_permissions($role_id)
    {
        $query = $this->db->get_where('roles', array('role_id' => $role_id));

        return $query->row_array()['permissions'];
    }

    public function get_login_history($user_id = null, $limit = 20)
    {
        if ($user_id != null) {
            $this->db->where('user_id', $user_id);
        }
        $this->db->limit($limit);
        $this->db->order_by('attempted_on', 'DESC');
        $query = $this->db->get('authentication_history');

        return $query->result_array();
    }

    public function get_user_from_mobile($mobile)
    {
        $query = $this->db->get_where('users', array('mobile' => $mobile));
        if (!empty($query->result_array())) {
            $response = array(
                'success' => true,
                'user_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function get_2fa_secret($user_id)
    {
        $this->db->select('2fa_secret');
        $query = $this->db->get_where('users', array('user_id' => $user_id));

        return $query->row_array();
    }

    public function get_user_from_email($email)
    {
        $query = $this->db->get_where('users', array('email' => "$email"));
        if (!empty($query->result_array())) {
            $response = array(
                'success' => true,
                'user_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function log_mobile_verification_otp_sent($mobile, $otp)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', array('mobile_verification_otp' => $otp));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_mobile_verification_status($mobile)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', array('mobile_verified' => 1));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_verification_hash($email, $random_hash)
    {
        $this->db->where('email', $email);
        $query = $this->db->update('users', array('email_verification_hash' => $random_hash));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_verification_status($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->update('users', array('email_verified' => 1));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_authentication($user_id, $update_data)
    {
        $this->db->where('user_id', $user_id);
        if ($this->db->update('users', $update_data)) {
            return true;
        } else {
            return false;
        }
    }

    public function log_otp_sent($mobile, $update_data){
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', $update_data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function log_login_otp_sent($mobile, $otp)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', array('login_otp' => $otp));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function reset_login_otp_details($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', array('login_otp' => 0));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_password_reset_hash($email, $hash, $valid_till)
    {
        $this->db->where('email', $email);
        $query = $this->db->update('users', array('password_reset_hash' => $hash, 'password_reset_hash_valid_till' => $valid_till));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_password($user_id, $new_password)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', array('password' => $new_password));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
