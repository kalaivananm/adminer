<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function get_statistics()
    {
        $this->db->select('COUNT(user_id) as total_users, SUM(CASE WHEN 2fa_enabled=1 THEN 1 ELSE 0 END) as 2fa_enabled');
        $query = $this->db->get('users');
        $users = $query->row_array();

        $this->db->select('COUNT(role_id) as total_roles');
        $query = $this->db->get('roles');
        $roles = $query->row_array();

        $this->db->select('COUNT(module_id) as total_modules');
        $query = $this->db->get('modules');
        $modules = $query->row_array();

        return array_merge($users, $roles, $modules);
    }
}
