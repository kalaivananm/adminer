<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Roles_model extends CI_Model
{
    public function get_all_roles()
    {
        $query = $this->db->get('roles');
        if ($query) {
            $response = array(
                'success' => true,
                'roles' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function get_roles_for_registration()
    {
        $query = $this->db->get_where('roles', array('register_allowed' => 1));
        if (!empty($query->result_array())) {
            $response = array(
                'success' => true,
                'roles' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function role_details($role_id)
    {
        $query = $this->db->get_where('roles', array('role_id' => $role_id));
        if ($query) {
            $response = array(
                'success' => true,
                'role_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function add_role($role_details)
    {
        $query = $this->db->insert('roles', $role_details);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_role($role_id, $new_role_details)
    {
        $this->db->where('role_id', $role_id);
        if ($this->db->update('roles', $new_role_details)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_role($role_id)
    {
        $this->db->where('role_id', $role_id);
        if ($this->db->delete('roles')) {
            return true;
        } else {
            return false;
        }
    }
}
