<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile_model extends CI_Model
{
    public function get_profile_details($user_id)
    {
        $this->db->select('users.*, roles.display_name as role_name');
        $this->db->join('roles', 'roles.role_id = users.role_id', 'LEFT_JOIN');
        $query = $this->db->get_where('users', array('user_id' => $user_id));

        return $query->row_array();
    }

    public function update_profile($user_id, $update_values)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', $update_values);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_verification_hash($email, $random_hash)
    {
        $this->db->where('email', $email);
        $query = $this->db->update('users', array('email_verification_hash' => $random_hash));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function log_otp_sent($mobile, $otp)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', array('mobile_verification_otp' => $otp));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_authentication($user_id, $update_data)
    {
        $this->db->where('user_id', $user_id);
        if ($this->db->update('users', $update_data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_mobile_verification_status($mobile)
    {
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', array('mobile_verified' => 1));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_2fa_secret($user_id, $secret)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', array('2fa_secret' => $secret));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function enable_2fa($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', array('2fa_enabled' => 1));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function disable_2fa($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', array('2fa_enabled' => 0, '2fa_secret' => ''));
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
