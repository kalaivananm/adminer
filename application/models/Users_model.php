<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users_model extends CI_Model
{
    public function get_all_users()
    {
        $query = $this->db->get('users');
        if ($query) {
            $response = array(
                'success' => true,
                'users' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function register_user($db_values)
    {
        $this->db->insert('users', $db_values);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function update_user($db_values, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('users', $db_values);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function user_details($user_id)
    {
        $query = $this->db->get_where('users', array('user_id' => $user_id));
        if ($query) {
            $response = array(
                'success' => true,
                'user_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function delete_user($user_id)
    {
        $this->db->where('user_id', $user_id);
        if ($this->db->delete('users')) {
            return true;
        } else {
            return false;
        }
    }

    public function update_otp_utilization($mobile, $update_data){
        $this->db->where('mobile', $mobile);
        $query = $this->db->update('users', $update_data);
        if($query){
            return true;
        } else {
            return false;
        }
    }
}
