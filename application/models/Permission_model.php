<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Permission_model extends CI_Model
{
    public function get_all_permissions()
    {
        $this->db->join('roles', 'roles.role_id = permissions.role_id');
        $this->db->join('modules', 'modules.module_id = permissions.module_id');
        $this->db->select('permissions.*, modules.display_name as module_name, roles.display_name as role_name');
        $query = $this->db->order_by('permissions.role_id')->get('permissions');
        if ($query) {
            $response = array(
                'success' => true,
                'permissions' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function get_permisson_modules()
    {
        $query = $this->db->get('modules');
        if ($query) {
            $response = array(
                'success' => true,
                'modules' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function permission_details($permission_id)
    {
        $query = $this->db->get_where('permissions', array('permission_id' => $permission_id));
        if ($query) {
            $response = array(
                'success' => true,
                'permission_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function get_permissions_by_role($role_id)
    {
        $this->db->join('modules m', 'm.module_id = p.module_id');
        $query = $this->db->get_where('permissions p', array('p.role_id' => $role_id));
        $results = array();
        foreach ($query->result_array() as $row) {
            $results[$row['name']] = $row;
        }

        return $results;
    }

    public function add_permission($permission_details)
    {
        $query = $this->db->insert('permissions', $permission_details);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_permission($permission_id, $permission_details)
    {
        $this->db->where('permission_id', $permission_id);
        if ($this->db->update('permissions', $permission_details)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_permission($permission_id)
    {
        $this->db->where('permission_id', $permission_id);
        if ($this->db->delete('permissions')) {
            return true;
        } else {
            return false;
        }
    }

    public function is_unique($role_id, $module_id, $permission_id)
    {
        if ($permission_id != null) {
            $this->db->where('permission_id!=', $permission_id);
        }
        $query = $this->db->get_where('permissions', array('role_id' => $role_id, 'module_id' => $module_id));
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
