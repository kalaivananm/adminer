<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings_model extends CI_Model
{
    public function retrieve_settings(array $settings)
    {
        $all_settings = array();
        foreach ($settings as $setting) {
            $query = $this->db->get($setting);
            if (!empty($query->result_array())) {
                $all_settings[$setting] = $query->row_array();
            }
        }

        return $all_settings;
    }

    public function update_settings($settings, $data)
    {
        $query = $this->db->update($settings, $data);
        return $query ? true : false;
    }
}
