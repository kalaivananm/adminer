<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Modules_model extends CI_Model
{
    public function get_all_modules()
    {
        $query = $this->db->get('modules');
        if ($query) {
            $response = array(
                'success' => true,
                'modules' => $query->result_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function module_details($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id));
        if ($query) {
            $response = array(
                'success' => true,
                'module_details' => $query->row_array(),
            );
        } else {
            $response = array(
                'success' => false,
            );
        }

        return $response;
    }

    public function add_module($module_details)
    {
        $query = $this->db->insert('modules', $module_details);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_module($module_id, $module_details)
    {
        $this->db->where('module_id', $module_id);
        if ($this->db->update('modules', $module_details)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_module($module_id)
    {
        $this->db->where('module_id', $module_id);
        if ($this->db->delete('modules')) {
            return true;
        } else {
            return false;
        }
    }
}
