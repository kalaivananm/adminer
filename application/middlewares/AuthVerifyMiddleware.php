<?php

class AuthVerifyMiddleware
{
    protected $controller;
    protected $ci;

    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci = $ci;
    }

    public function run()
    {
        if ($this->ci->session->userdata('session_auth_details') == null) {
            redirect('admin/authentication');
        }
    }
}