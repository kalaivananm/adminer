<?php

class PermissionsMiddleware
{
    protected $controller;
    protected $ci;

    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci = $ci;
        $this->module_name = $this->controller->module_name;
        $this->default_permission_check = array(
            'index' => 'view_permission',
            'add' => 'add_permission',
            'edit' => 'edit_permission',
            'delete' => 'delete_permission'
        );
        $this->user_permission_checks = isset($this->controller->permission_check) ? $this->controller->permission_check : array();
        $this->permission_checks = array_merge($this->default_permission_check, $this->user_permission_checks);
        $this->module_permissions = array();
    }

    public function run()
    {

        $this->permissions = $this->ci->session->userdata('session_auth_details')['permissions'];
        $this->view_data['permissions'] = $this->permissions;

        if (!$this->permissions or !isset($this->permissions[$this->module_name])) {
            die($this->ci->load->view('admin/no_access', $this->view_data, true));
        }

        foreach ($this->permissions[$this->module_name] as $key => $permission) {
            if (strpos($key, '_permission') !== false) {
                $this->module_permissions[$key] = $permission;
            };
        }

        $method = $this->controller->router->method;

        if (in_array($method, array_keys($this->permission_checks))) {
            if ($this->permissions[$this->module_name][$this->permission_checks[$method]] != 1) {
                die($this->ci->load->view('admin/no_access', $this->view_data, true));
            }
        }

    }
}