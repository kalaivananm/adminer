<!-- Notification messages -->
<?php
if($this->session->flashdata('notification')){
  $notification = $this->session->flashdata('notification');
  if($notification['success']){
  ?>
 <div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-check"></i> Success!</h5>
  <?php echo $notification['message']; ?>
</div>
<?php } else if($notification['success']==false && $notification['type']=='error'){ ?>
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-ban"></i> Error!</h5>
  <?php echo $notification['message']; ?>
</div>
<?php } else { ?>
  <div class="alert alert-warning alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-warning"></i> Warning!</h5>
  <?php echo $notification['message']; ?>
</div>
<?php } 
/** 
* In some cases (when loading the same view in get and post method), Codeigniter 
* flash data is not removed. The fix this, we manually removing the flash data
* on every page load
*/
unset($_SESSION['notification']);
} ?>

<!-- Form validation errors -->
<?php if(validation_errors()){ ?>
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-ban"></i> Error!</h5>
  <?php echo validation_errors('<li>','</li>'); ?>
</div>
<?php } ?>