<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <span class="nav-link"><b>Last Login:</b> <?php echo $this->session->userdata('session_auth_details')['last_login']; ?></span>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
        <i class="fa fa-user"></i> My Account
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="<?php echo site_url('admin/profile'); ?>" class="dropdown-item">Profile</a>
        <div class="dropdown-divider"></div>
        <?php if (isset($permissions['settings'])) { ?>
            <a href="<?php echo site_url('admin/settings'); ?>" class="dropdown-item">Settings</a>
        <?php 
      } ?>
        <a href="<?php echo site_url('admin/authentication/logout'); ?>" class="dropdown-item">Logout</a>
      </div>
    </li>
  </ul>
</nav>
<!-- /.navbar -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo site_url('admin/dashboard'); ?>" class="text-center brand-link">
    <span class="brand-text font-weight-light"><?php echo APP_NAME; ?></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <?php if (isset($permissions['dashboard'])) { ?>
          <li class="nav-item">
            <a href="<?php echo site_url('admin/dashboard'); ?>" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
        <?php 
      } ?>
      <?php if (isset($permissions['users'])) { ?>
        <li class="nav-item">
          <a href="<?php echo site_url('admin/users'); ?>" class="nav-link">
            <i class="nav-icon fa fa-user"></i>
            <p>
              Users
            </p>
          </a>
        </li>
        <?php 
      } ?>
        <?php if (isset($permissions['roles'])) { ?>
        <li class="nav-item">
          <a href="<?php echo site_url('admin/roles'); ?>" class="nav-link">
            <i class="nav-icon fa fa-user-plus"></i>
            <p>
              Roles
            </p>
          </a>
        </li>
        <?php 
      } ?>
        <?php if (isset($permissions['permissions'])) { ?>
        <li class="nav-item">
          <a href="<?php echo site_url('admin/permissions'); ?>" class="nav-link">
            <i class="nav-icon fa fa-asterisk"></i>
            <p>
              Permissions
            </p>
          </a>
        </li>
        <?php 
      } ?>
        <?php if (isset($permissions['modules'])) { ?>
        <li class="nav-item">
          <a href="<?php echo site_url('admin/modules'); ?>" class="nav-link">
            <i class="nav-icon fa fa-check-square-o"></i>
            <p>
              Modules
            </p>
          </a>
        </li>
        <?php 
      } ?>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>