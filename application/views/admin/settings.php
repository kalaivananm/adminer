<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Settings | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <!-- Navbar and Sidebar -->
	<?php require(__DIR__ . "/partial/sidebar.php"); ?>
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Settings</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
							<li class="breadcrumb-item active">Settings</li>
						</ol>
					</div>
				</div>
			</div><!-- /.container-fluid -->
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
                <?php require(__DIR__ . '/partial/notification_and_form_error.php'); ?>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header p-2">
								<ul class="nav nav-pills">
									<li class="nav-item"><a class="nav-link active" href="#authentication" data-toggle="tab">Authentication</a></li>
									<li class="nav-item"><a class="nav-link" href="#general" data-toggle="tab">General</a></li>
								</ul>
							</div>
							<div class="card-body">
								<div class="tab-content">
									<div class="active tab-pane" id="authentication">
										<?php echo form_open_multipart('admin/settings/authentication/'); ?>
											<div class="form-group">
												<label for="allow_reset_password" class="col-sm-4 control-label">Allow Resetting Password?</label>
												<div class="col-sm-10">
													<select class='form-control' id="allow_reset_password" name="allow_reset_password">
														<option <?= $auth_settings['allow_reset_password'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $auth_settings['allow_reset_password'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div>
                      <div class="form-group">
												<label for="otp_login_allowed" class="col-sm-4 control-label">OTP Login Allowed?</label>
												<div class="col-sm-10">
													<select class='form-control' id="otp_login_allowed" name="otp_login_allowed">
														<option <?= $auth_settings['otp_login_allowed'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $auth_settings['otp_login_allowed'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="max_otp_attempts" class="col-md-12 control-label">Max OTP Attempts</label>
												<div class="col-sm-10">
													<input type="number" value="<?php echo $auth_settings['max_otp_attempts']; ?>" class="form-control" id="max_otp_attempts" name="max_otp_attempts">
												</div>
											</div>
                      <div class="form-group">
												<label for="otp_block_time" class="col-md-12 control-label">OTP Block Time (in minutes)</label>
												<div class="col-sm-10">
													<input type="number" value="<?php echo $auth_settings['otp_block_time']; ?>" class="form-control" id="otp_block_time" name="otp_block_time">
												</div>
											</div>
                      <div class="form-group">
												<label for="max_failed_attempts" class="col-md-12 control-label">Max Failed Attempts <small>(After account will be disabled)</small></label>
												<div class="col-sm-10">
													<input type="number" value="<?php echo $auth_settings['max_failed_attempts']; ?>" class="form-control" id="max_failed_attempts" name="max_failed_attempts">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
									<div class="tab-pane" id="general">
									<?php echo form_open_multipart('admin/settings/general/'); ?>
											<div class="form-group">
												<label for="registration_allowed" class="col-sm-4 control-label">Registration Allowed?</label>
												<div class="col-sm-10">
													<select class='form-control' id="registration_allowed" name="registration_allowed">
														<option <?= $general_settings['registration_allowed'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $general_settings['registration_allowed'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="allow_remember_me" class="col-sm-4 control-label">Allow RememberMe?</label>
												<div class="col-sm-10">
													<select class='form-control' id="allow_remember_me" name="allow_remember_me">
														<option <?= $general_settings['allow_remember_me'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $general_settings['allow_remember_me'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div>
                      <div class="form-group">
												<label for="req_mobile_verification" class="col-sm-4 control-label">Mobile verification required?</label>
												<div class="col-sm-10">
													<select class='form-control' id="req_mobile_verification" name="req_mobile_verification">
														<option <?= $general_settings['req_mobile_verification'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $general_settings['req_mobile_verification'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div>
                      <div class="form-group">
												<label for="req_email_verification" class="col-sm-4 control-label">Email verification required?</label>
												<div class="col-sm-10">
													<select class='form-control' id="req_email_verification" name="req_email_verification">
														<option <?= $general_settings['req_email_verification'] == 1 ? 'selected' : ''; ?> value="1">Yes</option>
														<option <?= $general_settings['req_email_verification'] == 0 ? 'selected' : ''; ?> value="0">No</option>
													</select>
												</div>
											</div> 
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>
								</div>
								<!-- /.tab-content -->
							</div><!-- /.card-body -->
						</div>
						<!-- /.nav-tabs-custom -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> <?php echo APP_VERSION; ?>
    </div>
    <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
