<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Profile | <?php echo APP_NAME; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <!-- Navbar and Sidebar -->
    <?php require(__DIR__ . "/../partial/sidebar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
                            <li class="breadcrumb-item active">User Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header">
								<h3 class="card-title">
									<i class="fa fa-key"></i>
									2 Factor Authentication
								</h3>
							</div>
							<div class="card-body">
                                <?php require(__DIR__ . '/../partial/notification_and_form_error.php'); ?>
								<?php
        if ($tfa_enabled) { ?>
									<div class="alert alert-info alert-dismissible">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<h5><i class="icon fa fa-info"></i> How 2 Factor Authentication will work?</h5>
										<ul>
											<li>Download any TOTP Application (Ex: Google Authenticator) on your mobile and scan the QR code or enter the secret key </li>
											<li>The Application will generate a 6 digit number for every 30 seconds (Internet connection not required) </li>
											<li>Once your logged into our Application (Adminer), you will be prompted to enter a 6 digit code</li>
											<li>Open your TOTP Application (Ex: Google Authenticator) to get latest 6 digit TOTP code</li>
											<li>Enter the latest 6 digit code to finish the login</li>
										</ul>
									</div>
									<h3>1. Secret Key</h3>
									<p>In case of your device missing or data lost, you can use this secret key to generate the TOTP from your new device</p>
									<p><b>Secrect Key:</b> <i><?php echo $secret; ?> </i></p>
									<h3>2. Scan the code</h3>
									<p>Scan the below QR code in your application or enter the secret key <i><?php echo $secret; ?></i></p>
									<img src="<?php echo $qr_code_url; ?>">
									<br><br>
									<h3>3. Enter the 6 digit code</h3>
									<p>After scanning the QR image or entering the secret key, app will display 6 digit code</p>
									<?php echo form_open('admin/profile/two_factor_authentication/' . $status); ?>
									<label for="verification_code">Enter the code below</label>
									<input type="text" name="verification_code" id="verification_code" class="form-control col-2"><br>
									<input type="submit" value="Enable" class="btn btn-primary">
                                    <?php echo form_close(); ?>
								<?php 
    } else { ?>
                                    <?php echo form_open('admin/profile/two_factor_authentication/' . $status); ?>
									<h3>1. Enter the 6 digit code</h3>
									<p>Use the TOTP application which you have used to enable the Two Factor Acuthentication. It will display the 6 digit code</p>
									<label for="verification_code">Enter the code to disable</label>
									<input type="text" name="verification_code" id="verification_code" class="form-control col-2"><br>
									<input type="submit" value="Disable" class="btn btn-primary">
                                    <?php echo form_close(); ?>
								<?php 
    } ?>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> <?php echo APP_VERSION; ?>
        </div>
        <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
    </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
