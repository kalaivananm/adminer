<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Profile | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <!-- Navbar and Sidebar -->
	<?php require(__DIR__ . "/../partial/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Profile</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
							<li class="breadcrumb-item active">User Profile</li>
						</ol>
					</div>
				</div>
			</div><!-- /.container-fluid -->
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
                <?php require(__DIR__ . '/../partial/notification_and_form_error.php'); ?>
				<div class="row">

					<div class="col-md-4">

						<!-- Profile Image -->
						<div class="card card-primary card-outline">
							<div class="card-body box-profile">
								<div class="text-center">
									<?php
								if ($user_details['profile_image'] != '') {
									?>
										<img class="profile-user-img img-fluid img-circle" src="<?php echo base_url('uploads/profile/' . $user_details['profile_image']); ?>">
									<?php 
							} else { ?>
										<img class="profile-user-img img-fluid img-circle" src="<?php echo base_url('uploads/profile/default.png'); ?>">
									<?php 
							} ?>
								</div>

								<h3 class="profile-username text-center"><?php echo $user_details['first_name'] . " " . $user_details['last_name']; ?></h3>

								<p class="text-muted text-center"><?php echo $user_details['role_name']; ?></p>

								<ul class="list-group list-group-unbordered mb-3">
									<li class="list-group-item">
										<b>Email: </b>
										<?php
									if (!$user_details['email_verified']) {
										$link = "<a href='" . site_url('admin/profile/send_verification_email') . "' class='badge bg-primary'>Verify</a>";
									} else {
										$link = "<i aria-label='Verified' class='fa fa-check text-success'></i>";
									}
									?>
										<span class="float-right ml-1"><?php echo $link; ?></span>
										<a class="float-right"><?php echo $user_details['email']; ?> </a>
									</li>
									<li class="list-group-item">
										<b>Mobile Number: </b>
                                        <?php
																																							if (!$user_details['mobile_verified']) {
																																								$link = "<a href='" . site_url('admin/profile/verify_mobile') . "' class='badge bg-primary'>Verify</a>";
																																							} else {
																																								$link = "<i aria-label='Verified' class='fa fa-check text-success'></i>";
																																							}
																																							?>
										<span class="float-right ml-1"><?php echo $link; ?></span>
										<a class="float-right"><?php echo $user_details['mobile']; ?></a>
									</li>
									<li class="list-group-item">
										<b>Registered On: </b> <a class="float-right"><?php echo $user_details['registered_on']; ?></a>
									</li>
									<li class="list-group-item">
										<b>Last Failed Attempt: </b> <a class="float-right"><?php echo $user_details['last_attempt_on']; ?></a>
									</li>
									<li class="list-group-item">
										<b>Last Failed Attempt from: </b> <a class="float-right"><?php echo $user_details['last_attempt_ip']; ?></a>
									</li>
									<li class="list-group-item">
										<?php
									if ($user_details['2fa_enabled']) {
										$link = "<a class='badge bg-primary' href='" . site_url('admin/profile/two_factor_authentication/0') . "'>Disable</a>";
									} else {
										$link = "<a class='badge bg-primary' href='" . site_url('admin/profile/two_factor_authentication/1') . "'>Enable</a>";
									}
									?>
										<span class="float-right ml-1"><?php echo $link; ?></span>
										<b>2 Factor Authentication: </b>
									</li>
								</ul>

<!--								<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
						<!-- /.card -->
					</div>
					<!-- /.col -->
					<div class="col-md-8">
						<div class="card">
							<div class="card-header p-2">
								<ul class="nav nav-pills">
									<li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Settings</a></li>
									<li class="nav-item"><a class="nav-link" href="#activity" data-toggle="tab">Activity</a></li>

								</ul>
							</div><!-- /.card-header -->
							<div class="card-body">
								<div class="tab-content">

									<div class="active tab-pane" id="settings">

										<?php echo form_open_multipart('admin/profile/'); ?>
											<div class="form-group">
												<label for="first_name" class="col-md-12 control-label">First Name</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="first_name" name="first_name"  value="<?php echo $user_details['first_name']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="last_name" class="col-md-12 control-label">Last Name</label>

												<div class="col-sm-10">
													<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $user_details['last_name']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label for="profile_image" class="col-md-12 control-label">Profile Image</label>
												<div class="col-sm-10">
													<input type="file" class="form-control" id="profile_image" name="profile_image">
												</div>
											</div>
											<div class="form-group">
												<label for="current_password" class="col-md-12 control-label">Current Password</label>

												<div class="col-sm-10">
													<input type="password" class="form-control" id="current_password" name="current_password">
												</div>
											</div>
											<div class="form-group">
												<label for="new_password" class="col-md-12 control-label">New Password</label>
												<div class="col-sm-10">
													<small>(Leave it blank to use old password)</small>
													<input type="password" class="form-control" id="new_password" name="new_password">
												</div>
											</div>
											<div class="form-group">
												<label for="confirm_password" class="col-md-12 control-label">Confirm New Password</label>
												<div class="col-sm-10">
													<input type="password" class="form-control" id="confirm_password" name="confirm_password">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="submit" class="btn btn-primary">Update</button>
												</div>
											</div>
										<?php echo form_close(); ?>
									</div>

									<div class="tab-pane" id="activity">
										<table class="table table-bordered">
											<p>Last 20 login attempts</p>
											<tbody>
											<tr>
												<th style="width: 10px">#</th>
												<th>Attempted On</th>
												<th>Attempted IP</th>
												<th style="width: 40px">Status</th>
											</tr>
											<?php
										$i = 1;
										foreach ($login_history as $history) {
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $history['attempted_on']; ?></td>
												<td><?php echo $history['attempt_ip']; ?></td>
												<td>
													<?php echo ($history['status'] == 1) ? "<span class='badge badge-success'>success</span>" : "<span class='badge badge-danger'>failed</span>"; ?>
												</td>
											</tr>
											<?php $i++;
									} ?>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /.tab-content -->
							</div><!-- /.card-body -->
						</div>
						<!-- /.nav-tabs-custom -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> <?php echo APP_VERSION; ?>
    </div>
    <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
