<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Profile | <?php echo APP_NAME; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <!-- Navbar and Sidebar -->
    <?php require(__DIR__ . "/../partial/sidebar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
                            <li class="breadcrumb-item active">User Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="row">
                                <div class="card-body login-card-body">
                                    <?php require(__DIR__ . '/../partial/notification_and_form_error.php'); ?>
                                    <?php echo form_open('admin/profile/verify_mobile'); ?>
                                    <!-- Form value re population -->
                                    <?php
                                    if ($this->session->flashdata('form_repopulation')) {
                                        $form_repopulation = $this->session->flashdata('form_repopulation');
                                        if (isset($form_repopulation['email'])) {
                                            $email = $form_repopulation['email'];
                                        }
                                        if (isset($form_repopulation['password'])) {
                                            $password = $form_repopulation['password'];
                                        }
                                    }
                                    ?>
                                    <div class="form-group has-feedback">
                                        <label for="verification_code">Enter Verification Code</label>
                                        <input type="number" class="form-control" id="verification_code" name="verification_code">
                                    </div>
                                    <div class="row">
                                        <div class="col-8">
                                            <a href="">Resend OTP</a>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-4">
                                            <button type="submit" class="btn btn-primary btn-block">Verify</button>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                            <!-- /.login-card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> <?php echo APP_VERSION; ?>
        </div>
        <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
    </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
