<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Role | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar and Sidebar -->
  <?php require(__DIR__ . "/../partial/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/roles'); ?>">Roles</a></li>
              <li class="breadcrumb-item active">Add Role</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <?php require(__DIR__ . "/../partial/notification_and_form_error.php"); ?>
              <form method="POST" action="<?php echo site_url('admin/roles/add'); ?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="display_name">Display Name</label>
                    <input type="text" class="form-control" name="display_name" id="display_name">
                  </div>
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" readonly class="form-control" name="name" id="name">
                  </div>
                  <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status">
                      <option value="1">Active</option>
                      <option value="0">Disabled</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="register_allowed">Register Allowed?</label>
                    <select name="register_allowed" class="form-control" id="register_allowed">
                      <option value="1">Allowed</option>
                      <option value="0">Not allowed</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="login_allowed">Login Allowed?</label>
                    <select name="login_allowed" class="form-control" id="login_allowed">
                      <option value="1">Allowed</option>
                      <option value="0">Not allowed</option>
                    </select>
                  </div>
                   <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> <?php echo APP_VERSION; ?>
    </div>
    <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
<script>
var $display_name = $("#display_name");
var $name = $("#name");
$display_name.on('keyup', function(){
  var name = $display_name.val().replace(/\s+/g,'_').toLowerCase();
  $name.val(name);
});
</script>
</body>
</html>
