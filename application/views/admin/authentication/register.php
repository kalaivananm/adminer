<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Register | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">

<div class="register-box">
  <div class="register-logo">
    <?php echo APP_NAME; ?>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      
      
      <!-- Checking where any role available to register -->
      <?php if ($roles['success']) { ?>
        
      <p class="login-box-msg">Register a new membership <br><small class="m-auto">All the fields are required</small></p>
      <?php require(__DIR__ . "/../partial/notification_and_form_error.php"); ?>
      <?php echo form_open('admin/authentication/register'); ?>
        <div class="form-group has-feedback">
          <label for="first_name">First Name</label>
          <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo set_value('first_name'); ?>" >
        </div>
        <div class="form-group has-feedback">
          <label for="last_name">Last Name</label>
          <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo set_value('last_name'); ?>" >
        </div>
        <div class="form-group has-feedback">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" >
        </div>
        <div class="form-group has-feedback">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password" >
        </div>
        <div class="form-group has-feedback">
          <label for="confirm_password">Confirm Password</label>
          <input type="password" class="form-control" id="confirm_password" name="confirm_password" >
        </div>
        <div class="form-group has-feedback">
          <label for="mobile">Mobile</label>
          <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo set_value('mobile'); ?>" >
        </div>
        <div class="form-group has-feedback">
          <label for="role">Role</label>
          <select id="role" name="role" class="form-control">
            <?php foreach ($roles['roles'] as $key => $role) {
              $selected = ($role['name'] == set_value('role')) ? 'selected' : '';
              echo "<option value='" . $role['name'] . "' " . $selected . " >" . $role['display_name'] . "</option>";
            }; ?>
          </select>
        </div>

        <div class="row">

          <div class="col-8">
          <a href="<?php echo site_url('admin/authentication'); ?>" class="text-center">Already have a account? Login</a>
          </div>

          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      <?php echo form_close(); ?>

      <?php 
    } else {
      echo '<p class="text-center">Registration not allowed right now</p>';
    } ?>

      <br>
      
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
