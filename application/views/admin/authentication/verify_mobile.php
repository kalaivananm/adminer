<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Login | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css');?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">

<div class="login-box">
  <div class="login-logo">
    <?php echo APP_NAME; ?>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <br>
      <?php require(__DIR__.'/../partial/notification_and_form_error.php');?>
      <?php echo form_open('admin/authentication/verify_mobile/'.$mobile_encrypted);?>
      <!-- Form value re population -->
      <?php
      if($this->session->flashdata('form_repopulation')){
        $form_repopulation = $this->session->flashdata('form_repopulation');
        if(isset($form_repopulation['email'])){
          $email = $form_repopulation['email'];  
        }
        if(isset($form_repopulation['password'])){
          $password = $form_repopulation['password'];  
        }
      }
      ?>
        <div class="form-group has-feedback">
          <label for="verification_code">Enter Verification Code</label>
          <input type="number" class="form-control" id="verification_code" name="verification_code">
        </div>
        <div class="row">
          <div class="col-8">
           
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Verify</button>
          </div>
          <!-- /.col -->
        </div>
      <?php echo form_close();?>
  
      <br>
      <div class='row'>
        <div class="col-6 text-left">
          <a href="">Resend OTP</a>
        </div>
        <div class="col-6 text-right">
          <a href="<?php echo site_url('admin/authentication');?>"></i> Go back to Login</a>
        </div>
      </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js');?>"></script>
</body>
</html>
