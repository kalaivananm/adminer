<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Permissions | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Data Tables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap4.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
 
  <!-- Navbar and Sidebar -->
  <?php require(__DIR__ . "/../partial/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Permissions</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Permissions</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <?php require(__DIR__ . "/../partial/notification_and_form_error.php"); ?>
              <?php if($add_permission) { ?> 
              <div class="row float-sm-right mr-auto">
                <a href="<?php echo site_url('admin/permissions/add'); ?>" class="btn btn-primary">Add New Permission</a>
              </div>
              <br><br>
              <?php } ?>
              <div class="responsive-md">
                <table id="dataTable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Role</th>
                      <th>Module</th>
                      <th>View</th>
                      <th>Add</th>
                      <th>Edit</th>
                      <th>Delete</th>
                      <th>Special</th>
                      <?php if($edit_permission || $delete_permission) { ?> <th>Actions</th> <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i = 1;
                    $total = count($permissions_list);
                    foreach ($permissions_list as $permission) {
                      ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $permission['role_name']; ?></td>
                      <td><?php echo $permission['module_name']; ?></td>
                      <td><?php echo ($permission['view_permission']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-minus text-danger'></i>"; ?></td>
                      <td><?php echo ($permission['add_permission']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-minus text-danger'></i>"; ?></td>
                      <td><?php echo ($permission['edit_permission']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-minus text-danger'></i>"; ?></td>
                      <td><?php echo ($permission['delete_permission']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-minus text-danger'></i>"; ?></td>
                      <td><?php echo ($permission['special_permission']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-minus text-danger'></i>"; ?></td>
                      <?php if($edit_permission || $delete_permission) { ?>
                      <td>
                        <?php if($edit_permission) { ?><a href="<?php echo site_url('admin/permissions/edit/' . $permission['permission_id']); ?>" class="btn btn-primary btn-sm">Edit</a><?php } ?>
                        <?php if($delete_permission) { ?><button data-delete_link='<?php echo site_url('admin/permissions/delete/' . $permission['permission_id']); ?>' data-toggle="modal" data-target="#exampleModal" class="btn btn-danger btn-sm">Delete</button> <?php } ?>
                      </td>
                      <?php } ?>
                    </tr>
                    <?php $i++; } ?>
                  </tbody>
                  <?php if ($total > 10) { ?>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Role</th>
                      <th>Module</th>
                      <th>View</th>
                      <th>Add</th>
                      <th>Edit</th>
                      <th>Delete</th>
                      <th>Special</th>
                      <?php if($edit_permission || $delete_permission) { ?> <th>Actions</th> <?php } ?>
                    </tr>
                  </tfoot>
                  <?php } ?>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <?php echo APP_VERSION; ?>
    </div>
    <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
  </footer>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Do you want to delete this?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Do you want to delete this Permissions?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <a id="delete_link" href="#" class="btn btn-danger">Delete</a>
        </div>
      </div>
    </div>
  </div>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- Datatable -->
<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap4.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>

<script>
  $("#dataTable").dataTable();
  $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); 
  var delete_link = button.data('delete_link');
  var modal = $(this);
  modal.find('#delete_link').attr('href',delete_link);
})
</script>
</body>
</html>
