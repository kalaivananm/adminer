<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Permission | <?php echo APP_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/adminlte.min.css'); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar and Sidebar -->
  <?php require(__DIR__ . "/../partial/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Permission</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="<?php echo site_url('admin/permissions'); ?>">Permissions</a></li>
              <li class="breadcrumb-item active">Edit Permission</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <?php require(__DIR__ . "/../partial/notification_and_form_error.php"); ?>
              <form method="POST" action="<?php echo site_url('admin/permissions/edit/' . $permission_details['permission_id']); ?>">
                <input type="hidden" name="permission_id" value="<?php echo $permission_details['permission_id']; ?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="role_id">Role</label>
                    <select class="form-control" name="role_id" id="role_id">
                      <?php
                      foreach ($roles as $role) {
                        $selected = ($role['role_id'] == $permission_details['role_id']) ? 'selected' : '';
                        echo "<option " . $selected . " value='" . $role['role_id'] . "'>" . $role['display_name'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="module_id">Module</label>
                    <select class="form-control" name="module_id" id="module_id">
                      <?php
                      foreach ($modules as $module) {
                        $selected = ($module['module_id'] == $permission_details['module_id']) ? 'selected' : '';
                        echo "<option " . $selected . " value='" . $module['module_id'] . "'>" . $module['display_name'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="view_permission">View Permissions</label>
                    <select name="view_permission" class="form-control" id="view_permission">
                      <option <?php echo ($permission_details['view_permission'] == 1) ? 'selected' : ''; ?> value="1">Yes</option>
                      <option <?php echo ($permission_details['view_permission'] == 0) ? 'selected' : ''; ?> value="0">No</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="add_permission">Add Permissions</label>
                    <select name="add_permission" class="form-control" id="add_permission">
                      <option <?php echo ($permission_details['add_permission'] == 1) ? 'selected' : ''; ?> value="1">Yes</option>
                      <option <?php echo ($permission_details['add_permission'] == 0) ? 'selected' : ''; ?> value="0">No</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="edit_permission">Edit Permissions</label>
                    <select name="edit_permission" class="form-control" id="edit_permission">
                      <option <?php echo ($permission_details['edit_permission'] == 1) ? 'selected' : ''; ?> value="1">Yes</option>
                      <option <?php echo ($permission_details['edit_permission'] == 0) ? 'selected' : ''; ?> value="0">No</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="delete_permission">Delete Permissions</label>
                    <select name="delete_permission" class="form-control" id="delete_permission">
                      <option <?php echo ($permission_details['delete_permission'] == 1) ? 'selected' : ''; ?> value="1">Yes</option>
                      <option <?php echo ($permission_details['delete_permission'] == 0) ? 'selected' : ''; ?> value="0">No</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="special_permission">Special Permissions</label>
                    <select name="special_permission" class="form-control" id="special_permission">
                      <option <?php echo ($permission_details['special_permission'] == 1) ? 'selected' : ''; ?> value="1">Yes</option>
                      <option <?php echo ($permission_details['special_permission'] == 0) ? 'selected' : ''; ?> value="0">No</option>
                    </select>
                  </div>
                 
                   <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <!-- /.card-body -->
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> <?php echo APP_VERSION; ?>
    </div>
    <strong>Copyright &copy; 2019</strong> <a href="#"><?php echo APP_NAME; ?></a>. Theme by <a href="https://adminlte.io/">Admin LTE</a>.
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/adminlte.min.js'); ?>"></script>
</body>
</html>
