<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Adminer_Encryption extends CI_Encryption
{
    public function encode($string, $url_safe = true)
    {
        $ret = parent::encrypt($string);
        if ($url_safe) {
            $ret = strtr($ret, array('+' => '.', '=' => '-', '/' => '~'));
        }

        return $ret;
    }

    public function decode($string)
    {
        $string = strtr($string, array('.' => '+', '-' => '=', '~' => '/'));

        return parent::decrypt($string);
    }
}
