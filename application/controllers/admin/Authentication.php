<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authentication extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Authentication_model', 'Settings_model', 'Permission_model', 'Roles_model', 'Users_model'));
        $this->load->library(['form_validation', 'encryption', 'adminer_encryption']);
        $this->load->helper('cookie');
        $this->settings = $this->Settings_model->retrieve_settings(array('settings_authentication','settings_general'));
    }

    public function index()
    {
        if ($this->session->userdata('session_auth_details') != null) {
            redirect('admin/dashboard');
        } elseif ($this->session->userdata('temp_session_auth_details') != null) {
            redirect('admin/authentication/two_factor_authentication');
        }

        $view_data = array(
            'allow_reset_password' => $this->settings['settings_authentication']['allow_reset_password'],
            'otp_login_allowed' => $this->settings['settings_authentication']['otp_login_allowed'],
            'registration_allowed' => $this->settings['settings_general']['registration_allowed'],
            'allow_remember_me' => $this->settings['settings_general']['allow_remember_me'],
        );

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($this->settings['settings_general']['allow_remember_me']) {
                $token = get_cookie('remember_me_token');
                if ($token != null) {
                    $model_call = $this->Authentication_model->check_remember_me_token($token);
                    if ($model_call['status']) {
                        $user_details = $model_call['user_details'];
                        $this->auth($user_details);
                    }
                }
            }
            $this->load->view('admin/authentication/login', $view_data);
        }
        
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/login', $view_data);
            } else {
                $model_call = $this->Authentication_model->email_login($this->input->post('email'));
                if ($model_call['status']) {
                    $user_details = $model_call['user_details'];
                    $options = array(
                        'verify_password' => true,
                        'password' => $this->input->post('password')
                    );
                    $response = $this->auth($user_details, $options);

                    $notification_response = array(
                        'success' => $response['status'],
                        'type' => 'error',
                        'message' => $response['message']
                    );

                    $form_repopulation['email'] = $this->input->post('email');
                    // $form_repopulation['password'] = $this->input->post('password');
                    $this->session->set_flashdata('notification', $notification_response);
                    $this->session->set_flashdata('form_repopulation', $form_repopulation);
                    $this->load->view('admin/authentication/login', $view_data);
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Invalid email or password',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/authentication/login', $view_data);
                }
            }
        }
    }

    public function otp()
    {
        if (!$this->settings['settings_authentication']['otp_login_allowed']) {
            redirect('admin/authentication');
        }
        $view_data = array(
            'allow_remember_me' => $this->settings['settings_general']['allow_remember_me'],
            'registration_allowed' => $this->settings['settings_general']['registration_allowed'],
        );
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/authentication/login_otp', $view_data);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/login_otp', $view_data);
            } else {
                $mobile_encrypted = urlencode($this->adminer_encryption->encode($this->input->post('mobile')));
                $remember_me = $this->input->post('remember_me') ? 'true' : '';
                redirect('admin/authentication/otp_verify/' . $mobile_encrypted . '/' . $remember_me);
            }
        }
    }

    public function otp_verify($mobile_encrypted = null, $remember_me = false)
    {
        if ($mobile_encrypted == null) {
            redirect('admin/authentication');
        }
        $mobile = $this->adminer_encryption->decode(urldecode($mobile_encrypted));
        $view_data = array(
            'mobile_encrypted' => $mobile_encrypted,
            'remember_me' => $remember_me,
        );
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Authentication_model->get_user_from_mobile($mobile);
            if ($model_call['success']) {
                $user_details = $model_call['user_details'];
            } else {
                $flash_data = array(
                    'success' => false,
                    'type' => 'error',
                    'message' => 'Invalid mobile number',
                );
                $this->session->set_flashdata('notification', $flash_data);
                redirect('admin/authentication/otp');
            }
            $otp_utilization_result = otp_utilization_control($user_details, $this->settings);
            if($otp_utilization_result['status']){
                $otp_details = login_via_otp($mobile, $user_details['email']);
                $otp = $otp_details['otp'];
                $this->Authentication_model->log_login_otp_sent($mobile, $otp);
                $flash_data = array(
                    'success' => true,
                    'message' => 'Verification code sent successfully. Please enter your OTP below',
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            else{
                $flash_data = array(
                    'success' => false,
                    'type' => 'error',
                    'message' => $otp_utilization_result['message']
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            $this->load->view('admin/authentication/login_otp_verify', $view_data);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('verification_code', 'Verification Code', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/login_otp_verify', $view_data);
            } else {
                $model_call = $this->Authentication_model->get_user_from_mobile($mobile);
                if ($model_call['success']) {
                    $user_details = $model_call['user_details'];
                    $login_otp = $user_details['login_otp'];
                } else {
                    $flash_data = array(
                        'success' => false,
                        'message' => 'Unable to verify your OTP',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication/otp');
                }
                if ($this->input->post('verification_code') != 0 && $this->input->post('verification_code') == $login_otp) {
                    $response = $this->auth($user_details);
                    $notification_response = array(
                        'success' => $response['status'],
                        'type' => 'error',
                        'message' => $response['message']
                    );
                    $this->session->set_flashdata('notification', $notification_response);
                    $view_data['mobile_encrypted'] = $mobile_encrypted;
                    $view_data['remember_me'] = $remember_me;
                    $this->load->view('admin/authentication/login_otp_verify', $view_data);
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Incorrect verification code',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/authentication/login_otp_verify', $view_data);
                }
            }
        }
    }

    public function two_factor_authentication()
    {
        if ($this->session->userdata('temp_session_auth_details') == null) {
            redirect('admin/authentication');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/authentication/two_factor_authentication');
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('verification_code', 'Verification Code', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/two_factor_authentication');
            } else {
                $this->load->library('GoogleAuthenticator');
                $ga = new GoogleAuthenticator();
                $user_details = $this->Authentication_model->get_2fa_secret($this->session->userdata('temp_session_auth_details')['user_id']);
                $checkResult = $ga->verifyCode($user_details['2fa_secret'], $this->input->post('verification_code'), 1);
                if ($checkResult) {
                    $this->session->set_userdata('session_auth_details', $this->session->userdata('temp_session_auth_details'));
                    unset($_SESSION['temp_session_auth_details']);
                    redirect('admin/dashboard');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Incorrect verification code',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/authentication/two_factor_authentication');
                }
            }
        }
    }

    private function auth($user_details, $options = array())
    {
        $auth_role_id = $user_details['role_id'];
        $is_login_allowed = $this->Authentication_model->is_login_allowed($auth_role_id);
        $response = array();
        if (isset($options['verify_password']) && !password_verify($options['password'], $user_details['password'])) {
            $response = array(
                'status' => false,
                'message' => 'Incorrect email or password',
            );
        } elseif (!$is_login_allowed) {
            $response = array(
                'status' => false,
                'message' => 'Currently login is not allowed for you',
            );
        } elseif ($user_details['status'] != 1) {
            $response = array(
                'status' => false,
                'message' => 'Your account is currently disabled',
            );
        } elseif ($this->settings['settings_general']['req_mobile_verification'] && $user_details['mobile_verified'] != 1) {
            $mobile_encrypted = urlencode($this->adminer_encryption->encode($user_details['mobile']));
            $verification_link = site_url('admin/authentication/verify_mobile/' . $mobile_encrypted);
            $response = array(
                'status' => false,
                'message' => 'Your mobile number must be verified before login. Please <a href="' . $verification_link . '"">Verify your mobile</a>',
            );
        } elseif ($this->settings['settings_general']['req_email_verification'] && $user_details['email_verified'] != 1) {
            $email_encrypted = urlencode($this->adminer_encryption->encode($user_details['email']));
            $email_link = "<a href='" . site_url('admin/authentication/send_verification_email/' . $email_encrypted) . "'>Resend verification email</a>";
            $response = array(
                'status' => false,
                'message' => 'Your email must be verified before login. ' . $email_link,
            );
        } elseif ($user_details['failed_attempts'] > $this->settings['settings_authentication']['max_failed_attempts']) {
            $response = array(
                'status' => false,
                'message' => 'To many failed attempts. Try resetting your password',
            );
        } else {
            $auth_history = array(
                'user_id' => $user_details['user_id'],
                'attempted_on' => date('Y-m-d H:i:s'),
                'attempt_ip' => get_ip_address(),
                'status' => 1,
            );
            $this->Authentication_model->log_auth_history($auth_history);
            $success_attempt = array(
                'last_login_ip' => get_ip_address(),
                'last_login' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
            );
            $this->Authentication_model->log_success_attempt($user_details['user_id'], $success_attempt);
            if ($this->input->post('remember_me')) {
                $token = generateRandomString(60);
                set_cookie('remember_me_token', $token, 1209600);
                $this->Authentication_model->set_remember_me_token($user_details['user_id'], $token);
            }
            $permissions = $this->Permission_model->get_permissions_by_role($user_details['role_id']);
            $session_auth_details = array(
                'user_id' => $user_details['user_id'],
                'first_name' => $user_details['first_name'],
                'last_name' => $user_details['last_name'],
                'last_login' => $user_details['last_login'],
                'last_login_ip' => $user_details['last_login_ip'],
                'role_name' => $user_details['role_name'],
                'permissions' => $permissions,
            );

            if ($user_details['2fa_enabled']) {
                $this->session->set_userdata('temp_session_auth_details', $session_auth_details);
                redirect('admin/authentication/two_factor_authentication');
            } else {
                $this->session->set_userdata('session_auth_details', $session_auth_details);
            }
            redirect('admin/dashboard');
        }
        if ($user_details['failed_attempts'] <= $this->settings['settings_authentication']['max_failed_attempts']) {
            $failed_auth_attempt = array(
                'last_attempt_ip' => get_ip_address(),
                'last_attempt_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
            );
            $this->Authentication_model->log_failed_attempt($user_details['user_id'], $failed_auth_attempt);
            $auth_history = array(
                'user_id' => $user_details['user_id'],
                'attempted_on' => date('Y-m-d H:i:s'),
                'attempt_ip' => get_ip_address(),
                'status' => 0,
            );
            $this->Authentication_model->log_auth_history($auth_history);
        }

        return $response;
    }
    public function register()
    {
        if ($this->session->userdata('session_auth_details') != null) {
            redirect('admin/dashboard');
        }
        if (!$this->settings['settings_general']['registration_allowed']) {
            redirect('admin/authentication');
        }
        $view_data = array();
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $roles = $this->Roles_model->get_roles_for_registration();
            $view_data['roles'] = $roles;
            $this->load->view('admin/authentication/register', $view_data);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]', array('is_unique' => 'Given email is already registered'));
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|is_unique[users.mobile]');
            $this->form_validation->set_rules('role', 'Role', 'required');
            if ($this->form_validation->run() == false) {
                $roles = $this->Roles_model->get_roles_for_registration();
                $view_data['roles'] = $roles;
                $this->load->view('admin/authentication/register', $view_data);
            } else {
                $roles = $this->Roles_model->get_roles_for_registration();
                if ($roles['success'] && in_array($this->input->post('role'), array_column($roles['roles'], 'name'))) {
                    $_POST['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
                    $_POST['role_id'] = array_column($roles['roles'], 'role_id', 'name')[$this->input->post('role')];
                    $_POST['registered_ip'] = get_ip_address();
                    $_POST['registered_on'] = date('Y-m-d H:i:s');
                    $_POST['email_verification_hash'] = md5(uniqid(rand(), true));
                    $db_values = get_inputs(array('first_name', 'last_name', 'email', 'password', 'mobile', 'role_id', 'registered_on', 'registered_ip', 'email_verification_hash'), $this->input->post());
                    $model_call = $this->Users_model->register_user($db_values);
                    if ($model_call) {
                        if ($this->settings['settings_general']['req_email_verification']) {
                            $encrypted_email = urlencode($this->adminer_encryption->encode($this->input->post('email')));
                            $random_hash = md5(uniqid(rand(), true));
                            $model_call = $this->Authentication_model->update_email_verification_hash($this->input->post('email'), $random_hash);
                            if (!$model_call) {
                                $flash_data = array(
                                    'success' => false,
                                    'type' => 'error',
                                    'message' => 'Registration Successful. But we could not send verification email now',
                                );
                                $this->session->set_flashdata('notification', $flash_data);
                                redirect('admin/authentication');
                            }
                            $link = site_url('admin/authentication/verify_email/' . $encrypted_email . '/' . $random_hash);
                            send_email_verification_mail($this->input->post('email'), $link);
                        }
                        if ($this->settings['settings_general']['req_mobile_verification']) {
                            $mobile_encrypted = urlencode($this->adminer_encryption->encode($this->input->post('mobile')));
                            $mobile_verification_link = "<a href='" . site_url('admin/authentication/verify_mobile/' . $mobile_encrypted) . "'>Verify your mobile number</a>";
                            $flash_data = array(
                                'success' => true,
                                'message' => 'Registration Successful. ' . $mobile_verification_link . ' to begin the session',
                            );
                            $this->session->set_flashdata('notification', $flash_data);
                            redirect('admin/authentication');
                        } else {
                            $flash_data = array(
                                'success' => true,
                                'message' => 'Registration Successful',
                            );
                            $this->session->set_flashdata('notification', $flash_data);
                            redirect('admin/authentication');
                        }
                    } else {
                        $flash_data = array(
                            'success' => false,
                            'type' => 'error',
                            'message' => 'Could not register now. Sorry for the inconvenience',
                        );
                        $this->session->set_flashdata('notification', $flash_data);
                        redirect('admin/authentication/register');
                    }
                } else {
                    $view_data['roles'] = $roles;
                    $this->load->view('admin/authentication/register', $view_data);
                }
            }
        }
    }
    public function verify_mobile($mobile_encrypted = null)
    {
        if ($mobile_encrypted == null) {
            redirect('admin/authentication');
        }
        $mobile = $this->adminer_encryption->decode(urldecode($mobile_encrypted));
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Authentication_model->get_user_from_mobile($mobile);
            if ($model_call['success']) {
                $user_details = $model_call['user_details'];
            } else {
                redirect('admin/authentication');
            }
            if ($user_details['mobile_verified'] == 1) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'Your mobile number already verified. Please do login',
                );
                $this->session->set_flashdata('notification', $flash_data);
                redirect('admin/authentication');
            }
            $otp_utilization_result = otp_utilization_control($user_details, $this->settings);
            if($otp_utilization_result['status']){
                $otp_details = mobile_verification_otp($mobile, $user_details['email']);
                $otp = $otp_details['otp'];
                $this->Authentication_model->log_mobile_verification_otp_sent($mobile, $otp);
                $flash_data = array(
                    'success' => true,
                    'message' => 'Verification code sent successfully. Please enter your OTP below',
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            else{
                $flash_data = array(
                    'success' => false,
                    'type' => 'error',
                    'message' => $otp_utilization_result['message']
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            $view_data['mobile_encrypted'] = $mobile_encrypted;
            $this->load->view('admin/authentication/verify_mobile', $view_data);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('verification_code', 'Verification Code', 'trim|required');
            if ($this->form_validation->run() == false) {
                $view_data['mobile_encrypted'] = $mobile_encrypted;
                $this->load->view('admin/authentication/verify_mobile', $view_data);
            } else {
                $model_call = $this->Authentication_model->get_user_from_mobile($mobile);
                if ($model_call['success']) {
                    $user_details = $model_call['user_details'];
                    $mobile_verification_otp = $user_details['mobile_verification_otp'];
                } else {
                    $flash_data = array(
                        'success' => false,
                        'message' => 'Unable to verify your mobile number',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication');
                }
                if ($this->input->post('verification_code') != 0 && $this->input->post('verification_code') == $mobile_verification_otp) {
                    $this->Authentication_model->update_mobile_verification_status($mobile);
                    if ($this->settings['settings_general']['req_email_verification'] && $user_details['email_verified'] == 0) {
                        $email_encrypted = urlencode($this->adminer_encryption->encode($user_details['email']));
                        $email_verification_link = 'Your email id is not verified yet.  <a href="' . site_url('admin/authentication/send_verification_email/' . $email_encrypted) . '">Resend email verification link</a>';
                    } else {
                        $email_verification_link = '';
                    }
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Mobile number verified successfully. ' . $email_verification_link,
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Incorrect verification code',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $view_data['mobile_encrypted'] = $mobile_encrypted;
                    $this->load->view('admin/authentication/verify_mobile', $view_data);
                }
            }
        }
    }
    public function send_verification_email($encrypted_email = null)
    {
        if ($encrypted_email == null) {
            redirect('admin/authentication');
        }
        $email = $this->adminer_encryption->decode(urldecode($encrypted_email));
        $random_hash = md5(uniqid(rand(), true));
        $model_call = $this->Authentication_model->update_email_verification_hash($email, $random_hash);
        if (!$model_call) {
            $flash_data = array(
                'success' => false,
                'type' => 'error',
                'message' => 'Could not send verification email now',
            );
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/authentication');
        }
        $link = site_url('admin/authentication/verify_email/' . $encrypted_email . '/' . $random_hash);
        send_email_verification_mail($email, $link);
        $flash_data = array(
            'success' => true,
            'message' => 'Verification link sent successfully to your email',
        );
        $this->session->set_flashdata('notification', $flash_data);
        redirect('admin/authentication');
    }

    public function verify_email($encrypted_email, $hash)
    {
        $email = $this->adminer_encryption->decode(urldecode($encrypted_email));
        $model_call = $this->Authentication_model->get_user_from_email($email);
        if ($model_call['success']) {
            $user_details = $model_call['user_details'];
        } else {
            $flash_data = array(
                'success' => false,
                'type' => 'error',
                'message' => 'Invalid link. Could not verify',
            );
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/authentication');
        }
        if ($user_details['email_verification_hash'] == $hash) {
            $this->Authentication_model->update_email_verification_status($email);
            $flash_data = array(
                'success' => true,
                'message' => 'Email verified successfully',
            );
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/authentication');
        }
    }

    public function reset_password()
    {
        if (!$this->settings['settings_authentication']['allow_reset_password']) {
            redirect('admin/authentication');
        }
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/authentication/reset_password');
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/reset_password');
            } else {
                $model_call = $this->Authentication_model->get_user_from_email($this->input->post('email'));
                if ($model_call['success']) {
                    $user_details = $model_call['user_details'];
                    $random_hash = md5(uniqid(rand(), true));
                    $valid_till = date('Y-m-d H:i:s', strtotime('+60 min'));
                    $model_call = $this->Authentication_model->update_password_reset_hash($this->input->post('email'), $random_hash, $valid_till);
                    $encrypted_email = urlencode($this->adminer_encryption->encode($this->input->post('email')));
                    $link = "<a href='" . site_url('admin/authentication/verify_reset_password/' . $encrypted_email . '/' . $random_hash) . "'> Link </a>";
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Reset password link has been successfully sent to your email. The link will be valid for next <b>one hour</b>',
                    );
                    send_password_reset_email($this->input->post('email'), $link);
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/authentication/reset_password');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Could not send verification link',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/authentication/reset_password');
                }
            }
        }
    }

    public function verify_reset_password($encrypted_email = null, $hash = null)
    {
        if ($encrypted_email == null || $hash == null) {
            redirect('admin/authentication');
        }
        $email = $this->adminer_encryption->decode(urldecode($encrypted_email));
        if ($email) {
            $model_call = $this->Authentication_model->get_user_from_email($email);
            if ($model_call['success']) {
                $user_details = $model_call['user_details'];
                if ($user_details['password_reset_hash'] != $hash) {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Invalid link. Could not reset the password',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication/');
                } elseif (strtotime($user_details['password_reset_hash_valid_till']) < time()) {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Reset link password is expired',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication/');
                }
            } else {
                $flash_data = array(
                    'success' => false,
                    'type' => 'error',
                    'message' => 'Invalid link. Could not reset the password',
                );
                $this->session->set_flashdata('notification', $flash_data);
                redirect('admin/authentication/');
            }
        } else {
            $flash_data = array(
                'success' => false,
                'type' => 'error',
                'message' => 'Invalid link. Could not reset the password',
            );
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/authentication/');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/authentication/new_password');
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/authentication/new_password');
            } else {
                $new_password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
                $model_call = $this->Authentication_model->update_password($user_details['user_id'], $new_password);
                if ($model_call) {
                    $this->Authentication_model->update_password_reset_hash($user_details['email'], '', '0000-00-00 00:00:00');
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Password has been changed successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/authentication/');
                }
            }
        }
    }

    public function logout($profile_updated = 0)
    {
        delete_cookie('remember_me_token');
        unset($_SESSION['session_auth_details']);
        unset($_SESSION['temp_session_auth_details']);
        if ($profile_updated == 1) {
            $flash_data = array(
                'success' => true,
                'message' => 'Profile details updated successfully. Please login again',
            );
            $this->session->set_flashdata('notification', $flash_data);
        }
        redirect('admin/authentication');
    }
}