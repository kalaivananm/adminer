<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Profile_model', 'Settings_model', 'Authentication_model'));
        $this->load->library('form_validation');
        $this->view_data['permissions'] = $this->permissions = $this->session->userdata('session_auth_details')['permissions'];
        $this->user_id = $this->permissions = $this->session->userdata('session_auth_details')['user_id'];
        $this->settings = $this->Settings_model->retrieve_settings(array('settings_authentication'));
    }

    public function index()
    {
        $this->view_data['user_details'] = $this->Profile_model->get_profile_details($this->user_id);
        $this->view_data['login_history'] = $this->Authentication_model->get_login_history($this->user_id);

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/profile/profile', $this->view_data);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('current_password', 'Current Password', 'required');
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('new_password', 'New Password', 'min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'min_length[6]');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/profile/profile', $this->view_data);
            } else {
                $inputs = get_inputs(array('first_name', 'last_name', 'current_password', 'new_password', 'confirm_password'), $this->input->post());
                $user_details = $this->Profile_model->get_profile_details($this->user_id);
                if (!password_verify($inputs['current_password'], $user_details['password'])) {
                    $flashdata = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Current password is incorrect',
                    );
                    $this->session->set_flashdata('notification', $flashdata);
                    $this->load->view('admin/profile/profile', $this->view_data);
                } else {
                    if (isset($_FILES['profile_image']) && $_FILES['profile_image']['error'] == 0) {
                        $config['upload_path'] = 'uploads/profile/';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '2048';
                        $config['encrypt_name'] = true;
                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('profile_image')) {
                            $error = $this->upload->display_errors();
                            $flashdata = array(
                                'success' => false,
                                'type' => 'error',
                                'message' => $error,
                            );
                            $this->session->set_flashdata('notification', $flashdata);
                            exit($this->load->view('admin/profile/profile', $this->view_data, true));
                        } else {
                            $profile_image = $this->upload->data()['file_name'];
                        }
                    }
                    $update_values = array(
                        'first_name' => $inputs['first_name'],
                        'last_name' => $inputs['last_name'],
                    );
                    if (isset($profile_image)) {
                        $update_values['profile_image'] = $profile_image;
                        // Remove the old image
                        if ($user_details['profile_image'] != '') {
                            unlink('uploads/profile/' . $user_details['profile_image']);
                        }
                    }
                    if ($this->input->post('new_password') != null) {
                        $update_values['password'] = password_hash($this->input->post('new_password'), PASSWORD_BCRYPT);
                    }
                    $model_call = $this->Profile_model->update_profile($this->user_id, $update_values);
                    if ($model_call) {
                        if ($this->input->post('new_password')) {
                            redirect('admin/authentication/logout/1');
                        } else {
                            $flashdata = array(
                                'success' => true,
                                'message' => 'Profile details updated successfully',
                            );
                            $this->session->set_flashdata('notification', $flashdata);
                            redirect('admin/profile');
                        }
                    } else {
                        $flashdata = array(
                            'success' => false,
                            'type' => 'error',
                            'message' => 'Could not update the profile',
                        );
                        $this->session->set_flashdata('notification', $flashdata);
                        redirect('admin/profile');
                    }
                }
            }
        }
    }

    public function send_verification_email()
    {
        $this->load->library('encryption');
        $this->load->library('adminer_encryption');
        $user_details = $this->Profile_model->get_profile_details($this->user_id);
        $encrypted_email = urlencode($this->encryption->encode($user_details['email']));
        $email = $this->encryption->decode(urldecode($encrypted_email));
        // Recreating a random hash and updating in authentication record
        $random_hash = md5(uniqid(rand(), true));
        $model_call = $this->Profile_model->update_email_verification_hash($email, $random_hash);
        if (!$model_call) {
            $flash_data = array(
                'success' => false,
                'type' => 'error',
                'message' => 'Could not send verification email now',
            );
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/profile');
        }
        $link = site_url('admin/authentication/verify_email/' . $encrypted_email . '/' . $random_hash);
        send_email_verification_mail($email, $link);
        $flash_data = array(
            'success' => true,
            'message' => 'Verification link sent successfully to your email. Verify your email by clicking the link',
        );
        $this->session->set_flashdata('notification', $flash_data);
        redirect('admin/profile');
    }


    public function verify_mobile()
    {
        $user_details = $this->Profile_model->get_profile_details($this->user_id);
        $mobile = $user_details['mobile'];

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($user_details['mobile_verified'] == 1) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'Your mobile number is already verified',
                );
                $this->session->set_flashdata('notification', $flash_data);
                redirect('admin/profile');
            }

            $otp_utilization_check = otp_utilization_control($user_details, $this->settings);
            if($otp_utilization_check['status']){
                $otp_details = mobile_verification_otp($mobile, $user_details['email']);
                $otp = $otp_details['otp'];
                $this->Authentication_model->log_mobile_verification_otp_sent($mobile, $otp);
                $flash_data = array(
                    'success' => true,
                    'message' => 'Verification code sent successfully. Please enter your OTP below',
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            else{
                $flash_data = array(
                    'success' => false,
                    'type' => 'error',
                    'message' => $otp_utilization_check['message']
                );
                $this->session->set_flashdata('notification', $flash_data);
            }
            $this->view_data['mobile_encrypted'] = $mobile;
            $this->load->view('admin/profile/verify_mobile', $this->view_data);
        }

        // For POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('verification_code', 'Verification Code', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/profile/verify_mobile');
            } else {
                if ($this->input->post('verification_code') != 0 && $this->input->post('verification_code') == $user_details['mobile_verification_otp']) {
                    $this->Profile_model->update_mobile_verification_status($mobile);
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Mobile number verified successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/profile');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Incorrect verification code',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/profile/verify_mobile', $this->view_data);
                }
            }
        }
    }

    public function two_factor_authentication($status = 0)
    {
        $this->load->library('GoogleAuthenticator');
        $ga = new GoogleAuthenticator();
        $user_details = $this->Profile_model->get_profile_details($this->user_id);
        $this->view_data['status'] = $status;

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($status == 0) {
                $this->view_data['tfa_enabled'] = 0;
                $this->load->view('admin/profile/two_factor_authentication', $this->view_data);
            } elseif ($status == 1) {
                if (!$user_details['2fa_enabled']) {
                    $secret = $ga->createSecret(32);
                    $this->Profile_model->update_2fa_secret($this->user_id, $secret);
                    $qr_code_url = $ga->getQRCodeGoogleUrl('Adminer', $secret);
                    $this->view_data['tfa_enabled'] = 1;
                    $this->view_data['secret'] = $secret;
                    $this->view_data['qr_code_url'] = $qr_code_url;
                    $this->load->view('admin/profile/two_factor_authentication', $this->view_data);
                }
            }
        } 
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->view_data['tfa_enabled'] = $status;
            $this->form_validation->set_rules('verification_code', 'Verification Code', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->view_data['tfa_enabled'] = 1;
                $this->view_data['secret'] = $user_details['2fa_secret'];
                $qr_code_url = $ga->getQRCodeGoogleUrl('Adminer', $user_details['2fa_secret']);
                $this->view_data['qr_code_url'] = $qr_code_url;
                $this->load->view('admin/profile/two_factor_authentication', $this->view_data);
            } else {
                $checkResult = $ga->verifyCode($user_details['2fa_secret'], $this->input->post('verification_code'), 2);
                if ($checkResult) {
                    if ($status == 1) {
                        $this->Profile_model->enable_2fa($this->user_id);
                    } elseif ($status == 0) {
                        $this->Profile_model->disable_2fa($this->user_id);
                    }
                    $flash_data = array(
                        'success' => true,
                        'type' => 'success',
                        'message' => '2FA Enabled Successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/profile');
                } else {
                    $this->view_data['tfa_enabled'] = 1;
                    $this->view_data['secret'] = $user_details['2fa_secret'];
                    $qr_code_url = $ga->getQRCodeGoogleUrl('Adminer', $user_details['2fa_secret']);
                    $this->view_data['qr_code_url'] = $qr_code_url;
                    $flash_data = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Incorrect TOTP code',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    $this->load->view('admin/profile/two_factor_authentication', $this->view_data);
                }
            }
        }
    }
}
