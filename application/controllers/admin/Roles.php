<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Roles extends Adminer_Controller
{
    public $module_name = 'roles';

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Roles_model');
        $this->load->library('form_validation');
        $this->view_data['permissions'] = $this->permissions = $this->middlewares['permissions']->permissions;
        $this->view_data['module_name'] = $this->module_name;
        foreach ($this->middlewares['permissions']->module_permissions as $key => $permission) {
            $this->view_data[$key] = $permission;
        }
    }

    public function index()
    {
        $model_call = $this->Roles_model->get_all_roles();
        if ($model_call['success']) {
            $this->view_data['roles'] = $model_call['roles'];
        }

        $this->load->view('admin/roles/view', $this->view_data);
    }

    public function add()
    {
        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/roles/add', $this->view_data);
        }

        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('name', 'Role Name', 'trim|required|is_unique[roles.name]');
            $this->form_validation->set_rules('display_name', 'Display Name', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('register_allowed', 'Register Allowed?', 'required');
            $this->form_validation->set_rules('login_allowed', 'Login Allowed?', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/roles/add', $this->view_data);
            } else {
                $role_details = get_inputs(array('name', 'display_name', 'status', 'register_allowed', 'login_allowed'), $this->input->post());
                $model_call = $this->Roles_model->add_role($role_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'New role added successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/roles');
                } else {
                }
            }
        }
    }

    public function edit($role_id = null)
    {
        if ($role_id == null) {
            redirect('roles');
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Roles_model->role_details($role_id);
            if (!$model_call['success']) {
                redirect('roles');
            }
            $this->view_data['role_details'] = $model_call['role_details'];
            $this->load->view('admin/roles/edit', $this->view_data);
        }
        
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('name', 'Role Name', 'trim|required');
            $this->form_validation->set_rules('display_name', 'Display Name', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('register_allowed', 'Register Allowed?', 'required');
            $this->form_validation->set_rules('login_allowed', 'Login Allowed?', 'required');
            if ($this->form_validation->run() == false) {
                $model_call = $this->Roles_model->role_details($role_id);
                if (!$model_call['success']) {
                    redirect('roles');
                }
                $this->view_data['role_details'] = $model_call['role_details'];
                $this->load->view('admin/roles/edit', $this->view_data);
            } else {
                $new_role_details = get_inputs(array('name', 'display_name', 'status', 'register_allowed', 'login_allowed'), $this->input->post());
                $model_call = $this->Roles_model->update_role($role_id, $new_role_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Role details updated successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/roles');
                } else {
                }
            }
        }
    }

    public function delete($role_id)
    {
        if ($role_id == null) {
            redirect('roles');
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Roles_model->delete_role($role_id);
            if ($model_call) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'Role deleted successfully',
                );
            } else {
                $flash_data = array(
                    'success' => false,
                    'message' => 'Unable to delete the role!',
                );
            }
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/roles');
        }
    }
}
