<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users extends Adminer_Controller
{
    public $module_name = 'users';

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Users_model', 'Roles_model', 'Settings_model'));
        $this->load->library('form_validation');
        $this->view_data['permissions'] = $this->permissions = $this->middlewares['permissions']->permissions;
        $this->view_data['module_name'] = $this->module_name;
        foreach ($this->middlewares['permissions']->module_permissions as $key => $permission) {
            $this->view_data[$key] = $permission;
        }
        $this->settings = $this->Settings_model->retrieve_settings(array('settings_authentication','settings_general'));
    }

    public function index()
    {
        $model_call = $this->Users_model->get_all_users();
        if ($model_call['success']) {
            $this->view_data['users'] = $model_call['users'];
        }

        $this->load->view('admin/users/view', $this->view_data);
    }

    public function add()
    {
        $roles = $this->Roles_model->get_roles_for_registration();
        $this->view_data['roles'] = $roles;

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/users/add', $this->view_data);
        }

        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|is_unique[users.mobile]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('role', 'Role', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/users/add', $this->view_data);
            } else {
                $roles = $this->Roles_model->get_roles_for_registration();
                if ($roles['success'] && in_array($this->input->post('role'), array_column($roles['roles'], 'name'))) {
                    $_POST['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
                    $_POST['role_id'] = array_column($roles['roles'], 'role_id', 'name')[$this->input->post('role')];
                    $_POST['registered_ip'] = get_ip_address();
                    $_POST['registered_on'] = date('Y-m-d H:i:s');
                    $_POST['email_verification_hash'] = md5(uniqid(rand(), true));
                    $db_values = get_inputs(array('first_name', 'last_name', 'email', 'password', 'mobile', 'status', 'role_id', 'registered_on', 'registered_ip', 'email_verification_hash'), $this->input->post());
                    $model_call = $this->Users_model->register_user($db_values);
                    if ($model_call) {
                        if ($this->settings['settings_general']['req_email_verification']) {
                            $encrypted_email = urlencode($this->adminer_encryption->encode($this->input->post('email')));
                            $random_hash = md5(uniqid(rand(), true));
                            $model_call = $this->Authentication_model->update_email_verification_hash($this->input->post('email'), $random_hash);
                            if (!$model_call) {
                                $flash_data = array(
                                    'success' => false,
                                    'type' => 'error',
                                    'message' => 'User created Successfully. But unable to send verification email now',
                                );
                                $this->session->set_flashdata('notification', $flash_data);
                                redirect('admin/users');
                            }
                            $link = site_url('admin/authentication/verify_email/' . $encrypted_email . '/' . $random_hash);
                            send_email_verification_mail($this->input->post('email'), $link);
                        } else {
                            $flash_data = array(
                                'success' => true,
                                'message' => 'User created Successfully',
                            );
                            $this->session->set_flashdata('notification', $flash_data);
                            redirect('admin/users');
                        }
                    } else {
                        $flash_data = array(
                            'success' => false,
                            'type' => 'error',
                            'message' => 'Unable to create the user',
                        );
                        $this->session->set_flashdata('notification', $flash_data);
                        redirect('admin/users/add');
                    }
                } else {
                    $this->view_data['roles'] = $roles;
                    $this->load->view('admin/users', $this->view_data);
                }
            }
        }
    }

    public function edit($user_id = null)
    {
        if ($user_id == null) {
            redirect('admin/users');
        }

        $model_call = $this->Users_model->user_details($user_id);
        if (!$model_call['success']) {
            redirect('admin/users');
        }
        $this->view_data['user_details'] = $user_details = $model_call['user_details'];
        $roles = $this->Roles_model->get_roles_for_registration();
        $this->view_data['roles'] = $roles;


        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/users/edit', $this->view_data);
        }
        
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            if ($this->input->post('email') !== $user_details['email']) {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            }
            if ($this->input->post('mobile') !== $user_details['mobile']) {
                $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|is_unique[users.mobile]');
            }
            $this->form_validation->set_rules('password', 'Password', 'min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'min_length[6]');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('role', 'Role', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/users/edit', $this->view_data);
            } else {
                $roles = $this->Roles_model->get_roles_for_registration();
                if ($roles['success'] && in_array($this->input->post('role'), array_column($roles['roles'], 'name'))) {
                    if ($this->input->post('password')) {
                        $_POST['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
                    }
                    $_POST['role_id'] = array_column($roles['roles'], 'role_id', 'name')[$this->input->post('role')];
                    $db_values = get_inputs(array('first_name', 'last_name', 'email', 'password', 'mobile', 'status', 'role_id'), $this->input->post());
                    $model_call = $this->Users_model->update_user($db_values, $user_id);
                    if ($model_call) {
                        $flash_data = array(
                            'success' => true,
                            'message' => 'User details updated successfully',
                        );
                        $this->session->set_flashdata('notification', $flash_data);
                        redirect('admin/users');
                    } else {
                        $flash_data = array(
                            'success' => false,
                            'type' => 'error',
                            'message' => 'Unable to update the user details',
                        );
                        $this->session->set_flashdata('notification', $flash_data);
                        redirect('admin/users/edit', $this->view_data);
                    }
                } else {
                    $this->view_data['roles'] = $roles;
                    $this->load->view('admin/users', $this->view_data);
                }
            }
        }
    }

    public function delete($user_id = null)
    {
        if ($user_id == null) {
            redirect('admin/users');
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Users_model->delete_user($user_id);
            if ($model_call) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'User deleted successfully',
                );
            } else {
                $flash_data = array(
                    'success' => false,
                    'message' => 'User to delete the role!',
                );
            }
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/users');
        }
    }
}
