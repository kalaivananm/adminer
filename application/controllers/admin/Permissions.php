<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Permissions extends Adminer_Controller
{
    public $module_name = 'permissions';

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }


    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Permission_model', 'Roles_model', 'Modules_model'));
        $this->load->library('form_validation');

        $this->view_data['permissions'] = $this->permissions = $this->middlewares['permissions']->permissions;
        $this->view_data['module_name'] = $this->module_name;
        foreach ($this->middlewares['permissions']->module_permissions as $key => $permission) {
            $this->view_data[$key] = $permission;
        }
    }

    public function index()
    {
        $model_call = $this->Permission_model->get_all_permissions();
        if ($model_call['success']) {
            $this->view_data['permissions_list'] = $model_call['permissions'];
        }

        $model_call = $this->Permission_model->get_permisson_modules();
        if ($model_call['success']) {
            $this->view_data['modules'] = $model_call['modules'];
        }
        $this->load->view('admin/permissions/view', $this->view_data);
    }

    public function add()
    {
        $model_call = $this->Roles_model->get_all_roles();
        if ($model_call['success']) {
            $this->view_data['roles'] = $model_call['roles'];
        }
        $model_call = $this->Modules_model->get_all_modules();
        if ($model_call['success']) {
            $this->view_data['modules'] = $model_call['modules'];
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/permissions/add', $this->view_data);
        }

        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('role_id', 'Role Name', 'trim|required|callback_check_unique_permissions');
            $this->form_validation->set_rules('module_id', 'Module Name', 'trim|required');
            $this->form_validation->set_rules('view_permission', 'View Permission', 'required');
            $this->form_validation->set_rules('add_permission', 'Add Permission', 'required');
            $this->form_validation->set_rules('edit_permission', 'Edit Permission', 'required');
            $this->form_validation->set_rules('delete_permission', 'Delete Permission', 'required');
            $this->form_validation->set_rules('special_permission', 'Special Permission', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/permissions/add', $this->view_data);
            } else {
                $permission_details = get_inputs(array('role_id', 'module_id', 'view_permission', 'add_permission', 'edit_permission', 'delete_permission', 'special_permission'), $this->input->post());
                $model_call = $this->Permission_model->add_permission($permission_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Permission added successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/permissions');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'message' => 'Adding permission failed',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/permissions');
                }
            }
        }
    }

    function check_unique_permissions()
    {
        $role_id = $this->input->post('role_id');
        $module_id = $this->input->post('module_id');
        $permission_id = ($this->input->post('permission_id') != null) ? $this->input->post('permission_id') : null;
        $this->form_validation->set_message('check_unique_permissions', 'Permission is already set');

        return $this->Permission_model->is_unique($role_id, $module_id, $permission_id);
    }

    public function edit($permission_id = null)
    {
        if ($permission_id == null) {
            redirect('permissions');
        }

        $model_call = $this->Roles_model->get_all_roles();
        if ($model_call['success']) {
            $this->view_data['roles'] = $model_call['roles'];
        }
        $model_call = $this->Modules_model->get_all_modules();
        if ($model_call['success']) {
            $this->view_data['modules'] = $model_call['modules'];
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Permission_model->permission_details($permission_id);
            if (!$model_call['success']) {
                redirect('permissions');
            }
            $this->view_data['permission_details'] = $model_call['permission_details'];
            $this->load->view('admin/permissions/edit', $this->view_data);
        }
        
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('role_id', 'Role Name', 'trim|required|callback_check_unique_permissions');
            $this->form_validation->set_rules('module_id', 'Module Name', 'trim|required');
            $this->form_validation->set_rules('view_permission', 'View Permission', 'required');
            $this->form_validation->set_rules('add_permission', 'Add Permission', 'required');
            $this->form_validation->set_rules('edit_permission', 'Edit Permission', 'required');
            $this->form_validation->set_rules('delete_permission', 'Delete Permission', 'required');
            $this->form_validation->set_rules('special_permission', 'Special Permission', 'required');
            if ($this->form_validation->run() == false) {
                $model_call = $this->Permission_model->permission_details($permission_id);
                if (!$model_call['success']) {
                    redirect('permissions');
                }
                $this->view_data['permission_details'] = $model_call['permission_details'];
                $this->load->view('admin/permissions/edit', $this->view_data);
            } else {
                $permission_details = get_inputs(array('role_id', 'module_id', 'view_permission', 'add_permission', 'edit_permission', 'delete_permission', 'special_permission'), $this->input->post());
                $model_call = $this->Permission_model->update_permission($permission_id, $permission_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Permission details updated successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/permissions');
                } else {
                    $flash_data = array(
                        'success' => false,
                        'message' => 'Updating permission is failed',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/permissions');
                }
            }
        }
    }

    public function delete($permission_id)
    {
        if ($permission_id == null) {
            redirect('permissions');
        }

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Permission_model->delete_permission($permission_id);
            if ($model_call) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'Permission deleted successfully',
                );
            } else {
                $flash_data = array(
                    'success' => false,
                    'message' => 'Unable to delete the Permission!',
                );
            }
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/permissions');
        }
    }
}
