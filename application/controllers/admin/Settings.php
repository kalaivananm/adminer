<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends Adminer_Controller
{
    public $module_name = 'settings';
    public $permission_check = array(
        'authentication' => 'edit_permission'
    );

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Settings_model');
        $this->load->library('form_validation');

        $settings = $this->Settings_model->retrieve_settings(array('settings_authentication','settings_general'));
        $this->permissions = $this->middlewares['permissions']->permissions;

        $this->view_data = array(
            'permissions' => $this->permissions,
            'module_name' => $this->module_name,
            'auth_settings' => $settings['settings_authentication'],
            'general_settings' => $settings['settings_general']
        );
    }

    public function index()
    {
        $this->load->view('admin/settings', $this->view_data);
    }

    public function authentication()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('allow_reset_password', 'Allow Reset Password', 'required');
            $this->form_validation->set_rules('otp_login_allowed', 'OTP Login Allowed', 'required');
            $this->form_validation->set_rules('max_otp_attempts', 'Max OTP Attempts', 'required');
            $this->form_validation->set_rules('otp_block_time', 'OTP Block Time', 'required');
            $this->form_validation->set_rules('max_failed_attempts', 'Maximum Failed Attempts', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/settings', $this->view_data);
            } else {
                $inputs = get_inputs(array('allow_reset_password', 'otp_login_allowed', 'max_otp_attempts', 'otp_block_time', 'registration_allowed', 'allow_remember_me', 'req_mobile_verification', 'req_email_verification', 'max_failed_attempts'), $this->input->post());
                $model_call = $this->Settings_model->update_settings('settings_authentication', $inputs);
                if ($model_call) {
                    $flashdata = array(
                        'success' => true,
                        'message' => 'Authentication settings updated successfully',
                    );
                    $this->session->set_flashdata('notification', $flashdata);
                    redirect('admin/settings');
                } else {
                    $flashdata = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Could not update authentication settings',
                    );
                    $this->session->set_flashdata('notification', $flashdata);
                    redirect('admin/settings');
                }
            }
        }
    }


    public function general()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('registration_allowed', 'Registration Allowed', 'required');
            $this->form_validation->set_rules('allow_remember_me', 'Allow Remember Me', 'required');
            $this->form_validation->set_rules('req_mobile_verification', 'Required Mobile Verification', 'required');
            $this->form_validation->set_rules('req_email_verification', 'Required Email Verification', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/settings', $this->view_data);
            } else {
                $inputs = get_inputs(array('registration_allowed', 'allow_remember_me', 'req_mobile_verification', 'req_email_verification'), $this->input->post());
                $model_call = $this->Settings_model->update_settings('settings_general', $inputs);
                if ($model_call) {
                    $flashdata = array(
                        'success' => true,
                        'message' => 'General settings updated successfully',
                    );
                    $this->session->set_flashdata('notification', $flashdata);
                    redirect('admin/settings');
                } else {
                    $flashdata = array(
                        'success' => false,
                        'type' => 'error',
                        'message' => 'Could not update general settings',
                    );
                    $this->session->set_flashdata('notification', $flashdata);
                    redirect('admin/settings');
                }
            }
        }
    }
}