<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Adminer_Controller
{
    public $module_name = 'dashboard';

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Authentication_model', 'Dashboard_model'));

        $this->view_data['permissions'] = $this->permissions = $this->middlewares['permissions']->permissions;
        $this->view_data['module_name'] = $this->module_name;
    }

    public function index()
    {
        $login_history = $this->Authentication_model->get_login_history(null, 6);
        $statistics = $this->Dashboard_model->get_statistics();
        $view_data = array(
            'permissions' => $this->permissions,
            'login_history' => $login_history,
            'statistics' => $statistics
        );
        $this->load->view('admin/dashboard.php', $view_data);
    }
}
