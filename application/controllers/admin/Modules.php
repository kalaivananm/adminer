<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Modules extends Adminer_Controller
{
    public $module_name = 'modules';

    protected function middleware()
    {
        return array('auth_verify', 'permissions');
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Modules_model');
        $this->load->library('form_validation');
        $this->view_data['permissions'] = $this->permissions = $this->middlewares['permissions']->permissions;
        $this->view_data['module_name'] = $this->module_name;
        foreach ($this->middlewares['permissions']->module_permissions as $key => $permission) {
            $this->view_data[$key] = $permission;
        }
    }

    public function index()
    {
        $model_call = $this->Modules_model->get_all_modules();
        if ($model_call['success']) {
            $this->view_data['modules'] = $model_call['modules'];
        }
        $this->load->view('admin/modules/view', $this->view_data);
    }

    public function add()
    {

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/modules/add', $this->view_data);
        }

        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('name', 'Module Name', 'trim|required|is_unique[modules.name]');
            $this->form_validation->set_rules('display_name', 'Display Name', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/modules/add', $this->view_data);
            } else {
                $module_details = get_inputs(array('name', 'display_name'), $this->input->post());
                $model_call = $this->Modules_model->add_module($module_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'New Module added successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/modules');
                } else {
                }
            }
        }
    }

    public function edit($module_id = null)
    {
        if ($module_id == null) {
            redirect('modules');
        }

        $model_call = $this->Modules_model->module_details($module_id);
        if (!$model_call['success']) {
            redirect('modules');
        }
        $this->view_data['module_details'] = $model_call['module_details'];

        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->load->view('admin/modules/edit', $this->view_data);
        }
        
        // FOR POST METHOD
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules('name', 'Role Name', 'trim|required');
            $this->form_validation->set_rules('display_name', 'Display Name', 'trim|required');
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/modules/edit', $this->view_data);
            } else {
                $module_details = get_inputs(array('name', 'display_name'), $this->input->post());
                $model_call = $this->Modules_model->update_module($module_id, $module_details);
                if ($model_call) {
                    $flash_data = array(
                        'success' => true,
                        'message' => 'Module details updated successfully',
                    );
                    $this->session->set_flashdata('notification', $flash_data);
                    redirect('admin/modules');
                } else {
                }
            }
        }
    }

    public function delete($module_id)
    {
        if ($module_id == null) {
            redirect('modules');
        }
        
        // FOR GET METHOD
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $model_call = $this->Modules_model->delete_module($module_id);
            if ($model_call) {
                $flash_data = array(
                    'success' => true,
                    'message' => 'Module deleted successfully',
                );
            } else {
                $flash_data = array(
                    'success' => false,
                    'message' => 'Unable to delete the Module!',
                );
            }
            $this->session->set_flashdata('notification', $flash_data);
            redirect('admin/modules');
        }
    }
}
