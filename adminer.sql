-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 10:52 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adminer`
--

-- --------------------------------------------------------

--
-- Table structure for table `authentication_history`
--

CREATE TABLE `authentication_history` (
  `authentication_history_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attempted_on` datetime NOT NULL,
  `attempt_ip` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authentication_history`
--

INSERT INTO `authentication_history` (`authentication_history_id`, `user_id`, `attempted_on`, `attempt_ip`, `status`) VALUES
(108, 12, '2019-02-17 14:11:57', '::1', 1),
(109, 12, '2019-02-17 14:13:50', '::1', 1),
(110, 12, '2019-02-17 14:27:04', '::1', 1),
(111, 12, '2019-02-17 21:31:39', '::1', 0),
(112, 12, '2019-02-17 21:31:45', '::1', 0),
(115, 12, '2019-02-17 23:53:07', '::1', 1),
(116, 12, '2019-02-17 23:57:44', '::1', 1),
(117, 12, '2019-02-17 23:59:02', '::1', 1),
(118, 12, '2019-02-18 00:06:38', '::1', 1),
(119, 12, '2019-02-18 00:07:14', '::1', 1),
(120, 12, '2019-02-18 00:08:53', '::1', 1),
(121, 12, '2019-02-18 00:09:44', '::1', 1),
(122, 13, '2019-02-18 00:17:56', '::1', 1),
(123, 12, '2019-02-18 00:36:30', '::1', 1),
(124, 13, '2019-02-18 00:36:38', '::1', 1),
(125, 13, '2019-02-18 14:59:01', '127.0.0.1', 1),
(126, 12, '2019-02-18 15:01:49', '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('e3c232fac07698aaaae577f83637dca50624e9f3', '127.0.0.1', 1550480755, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535303438303731353b5f5f63695f766172737c613a313a7b733a31323a226e6f74696669636174696f6e223b733a333a226f6c64223b7d),
('9731bc480589a7a707ae5ed6c6f5db2951cfa7c9', '127.0.0.1', 1550481164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535303438313132333b),
('de88e289e6fe2cf52cf6b550bd65e65ce75c7f6f', '127.0.0.1', 1550483458, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535303438333432343b73657373696f6e5f617574685f64657461696c737c613a373a7b733a373a22757365725f6964223b733a323a223132223b733a31303a2266697273745f6e616d65223b733a31303a224b616c616976616e616e223b733a393a226c6173745f6e616d65223b733a393a224d7574687573616d79223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031392d30322d31382030303a33363a3330223b733a31333a226c6173745f6c6f67696e5f6970223b733a333a223a3a31223b733a393a22726f6c655f6e616d65223b733a31313a2253757065722041646d696e223b733a31313a227065726d697373696f6e73223b613a363a7b733a393a2264617368626f617264223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a313a2231223b733a393a226d6f64756c655f6964223b733a313a2231223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a393a2264617368626f617264223b733a31323a22646973706c61795f6e616d65223b733a393a2244617368626f617264223b7d733a353a22726f6c6573223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a313a2232223b733a393a226d6f64756c655f6964223b733a313a2232223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a353a22726f6c6573223b733a31323a22646973706c61795f6e616d65223b733a353a22526f6c6573223b7d733a373a226d6f64756c6573223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a313a2233223b733a393a226d6f64756c655f6964223b733a313a2233223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a373a226d6f64756c6573223b733a31323a22646973706c61795f6e616d65223b733a373a224d6f64756c6573223b7d733a31313a227065726d697373696f6e73223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a313a2234223b733a393a226d6f64756c655f6964223b733a313a2234223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a31313a227065726d697373696f6e73223b733a31323a22646973706c61795f6e616d65223b733a31313a225065726d697373696f6e73223b7d733a353a227573657273223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a323a223137223b733a393a226d6f64756c655f6964223b733a313a2239223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a353a227573657273223b733a31323a22646973706c61795f6e616d65223b733a353a225573657273223b7d733a383a2273657474696e6773223b613a31303a7b733a31333a227065726d697373696f6e5f6964223b733a323a223138223b733a393a226d6f64756c655f6964223b733a323a223130223b733a373a22726f6c655f6964223b733a313a2231223b733a31353a22766965775f7065726d697373696f6e223b733a313a2231223b733a31343a226164645f7065726d697373696f6e223b733a313a2231223b733a31353a22656469745f7065726d697373696f6e223b733a313a2231223b733a31373a2264656c6574655f7065726d697373696f6e223b733a313a2231223b733a31383a227370656369616c5f7065726d697373696f6e223b733a313a2231223b733a343a226e616d65223b733a383a2273657474696e6773223b733a31323a22646973706c61795f6e616d65223b733a383a2253657474696e6773223b7d7d7d);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `display_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `name`, `display_name`) VALUES
(1, 'dashboard', 'Dashboard'),
(2, 'roles', 'Roles'),
(3, 'modules', 'Modules'),
(4, 'permissions', 'Permissions'),
(9, 'users', 'Users'),
(10, 'settings', 'Settings');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `view_permission` tinyint(4) NOT NULL,
  `add_permission` tinyint(4) NOT NULL,
  `edit_permission` tinyint(4) NOT NULL,
  `delete_permission` tinyint(4) NOT NULL,
  `special_permission` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `module_id`, `role_id`, `view_permission`, `add_permission`, `edit_permission`, `delete_permission`, `special_permission`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 1, 1, 1, 1),
(3, 3, 1, 1, 1, 1, 1, 1),
(4, 4, 1, 1, 1, 1, 1, 1),
(17, 9, 1, 1, 1, 1, 1, 1),
(18, 10, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `register_allowed` tinyint(4) NOT NULL,
  `login_allowed` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `display_name`, `register_allowed`, `login_allowed`, `status`) VALUES
(1, 'super_admin', 'Super Admin', 0, 1, 1),
(8, 'admin', 'Admin', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_authentication`
--

CREATE TABLE `settings_authentication` (
  `id` int(11) NOT NULL,
  `allow_reset_password` tinyint(4) NOT NULL,
  `otp_login_allowed` tinyint(4) NOT NULL,
  `max_otp_attempts` int(11) NOT NULL,
  `otp_block_time` int(11) NOT NULL,
  `registration_allowed` tinyint(4) NOT NULL,
  `allow_remember_me` tinyint(4) NOT NULL,
  `req_mobile_verification` tinyint(4) NOT NULL,
  `req_email_verification` tinyint(4) NOT NULL,
  `max_failed_attempts` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_authentication`
--

INSERT INTO `settings_authentication` (`id`, `allow_reset_password`, `otp_login_allowed`, `max_otp_attempts`, `otp_block_time`, `registration_allowed`, `allow_remember_me`, `req_mobile_verification`, `req_email_verification`, `max_failed_attempts`) VALUES
(1, 1, 1, 5, 1, 1, 1, 1, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `settings_general`
--

CREATE TABLE `settings_general` (
  `id` int(11) NOT NULL,
  `req_mobile_verification` tinyint(4) NOT NULL,
  `req_email_verification` tinyint(4) NOT NULL,
  `registration_allowed` tinyint(4) NOT NULL,
  `allow_remember_me` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_general`
--

INSERT INTO `settings_general` (`id`, `req_mobile_verification`, `req_email_verification`, `registration_allowed`, `allow_remember_me`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_verified` tinyint(4) NOT NULL,
  `password` varchar(150) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `mobile_verified` tinyint(4) NOT NULL,
  `mobile_verification_otp` int(11) NOT NULL,
  `login_otp` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `otp_sent` int(11) NOT NULL,
  `last_otp_sent_on` datetime DEFAULT NULL,
  `profile_image` varchar(300) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `2fa_enabled` tinyint(4) NOT NULL,
  `2fa_secret` varchar(250) NOT NULL,
  `remember_me_token` varchar(50) NOT NULL,
  `registered_ip` int(11) NOT NULL,
  `registered_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email_verification_hash` varchar(150) NOT NULL,
  `password_reset_hash` varchar(150) NOT NULL,
  `password_reset_hash_valid_till` datetime NOT NULL,
  `failed_attempts` int(11) NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_attempt_on` datetime DEFAULT NULL,
  `last_attempt_ip` varchar(50) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `email_verified`, `password`, `mobile`, `mobile_verified`, `mobile_verification_otp`, `login_otp`, `role_id`, `otp_sent`, `last_otp_sent_on`, `profile_image`, `status`, `2fa_enabled`, `2fa_secret`, `remember_me_token`, `registered_ip`, `registered_on`, `email_verification_hash`, `password_reset_hash`, `password_reset_hash_valid_till`, `failed_attempts`, `updated_on`, `last_attempt_on`, `last_attempt_ip`, `last_login`, `last_login_ip`) VALUES
(12, 'Kalaivanan', 'Muthusamy', 'kalaivanan.muthusamy97@gmail.com', 1, '$2y$10$jUYgBUoz6QbguXi6UyfZ5eRTAZHQtumg2auu77wlwnCCY6wtH7c4i', '08489099849', 1, 249908, 944271, 1, 1, '2019-02-18 14:42:03', '', 1, 0, 'RY4YJSTAAPBWMZ2RTHD4PXYTLQQJN2M3', '', 0, '2019-02-17 08:40:46', '243275b5a63b1814ef12ffc7070eee2c', 'e18d38f5aaae99e3fb5480673bfbe048', '2019-02-17 15:23:15', 0, '2019-02-18 15:01:49', '2019-02-17 21:31:45', '::1', '2019-02-18 15:01:49', '127.0.0.1'),
(13, 'Kalaivanan', 'Muthusamy', 'kalaivanan.muthusamy.97@gmail.com', 1, '$2y$10$gLVTBHraiabc8f7ArqeK5OZpVZMJEksB2hlqGvLMs4.1sQJCMXHkK', '8489099849', 1, 518359, 858205, 8, 1, '2019-02-18 14:58:43', '12a02fd760aeb2603f89e87748241f16.jpg', 1, 0, 'UD7OKG2JMKGVTXSBHLGDNIQRB3I43MZL', '', 0, '2019-02-17 18:31:35', '02c38517feea7faab4dec592f3713338', '', '0000-00-00 00:00:00', 0, '2019-02-18 14:59:01', NULL, '', '2019-02-18 14:59:01', '127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authentication_history`
--
ALTER TABLE `authentication_history`
  ADD PRIMARY KEY (`authentication_history_id`),
  ADD KEY `authentication_history.auth_id` (`user_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `permissions.role_id` (`role_id`),
  ADD KEY `permissions.module_id` (`module_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `settings_authentication`
--
ALTER TABLE `settings_authentication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_general`
--
ALTER TABLE `settings_general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `users.role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authentication_history`
--
ALTER TABLE `authentication_history`
  MODIFY `authentication_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings_authentication`
--
ALTER TABLE `settings_authentication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings_general`
--
ALTER TABLE `settings_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authentication_history`
--
ALTER TABLE `authentication_history`
  ADD CONSTRAINT `authentication_history.auth_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions.module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `permissions.role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users.role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
